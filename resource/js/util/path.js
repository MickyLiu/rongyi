(function(window) {


	var _currentPathList = [];
    
    var stroke,  sec = 0;


	//清除路径圆点
	var clearPath = function() {
		for (var i = 0, len = _currentPathList.length; i < len; i++) {
            if (_currentPathList[i]) {
                _currentPathList[i].remove();
            }
        }
	};



   var buildPath = function (path, paper, callback) {

        var cmds = [], s = path.shift();

        cmds.push("M" + s.join(' '));

        path.forEach(function (i) { cmds.push("L" + i.join(' ')); });

        return paper.path(cmds).attr({ "stroke-width": 1, "stroke": "#060", "stroke-opacity": 0 });
    }


	//画路径
	var drawPath = function(paper, path, callback) {
        clearPath();
        Path.stroke=Path.buildPath(path,paper);
        Path.sec=0;
        Path.paper=paper;
        clearInterval(spinTimer);

        /*
        var pathLength = path.length;
        var drawPathInterval = setInterval(function() {
        	i += 17;
        	if (i >= pathLength) {
        		clearInterval(drawPathInterval);
        		if ($.isFunction(callback)) {
        			callback.call(this);
        		}
        		return false;
        	}
        	try {
        		var t = path[i];
	        	var temp = container.circle(t[0], t[1], 4).attr({fill : 'red','stroke':'#fff'});
                temp.transform(transformParam);
	        	temp.attr('opacity', 0).animate({ opacity: 1.0 }, 500);
	        	 // _currentShownDiv.find('g.viewport').append(temp);
	        	 $('#' + sId).find('svg').append(temp);
	            _currentPathList.push(temp);
        	} catch (e) {
        		console.log(i);
        	}
        	
        }, 150);
        */
       
        //重复绘制动作
        var spinTimer=setInterval(function () {
            if (Path.sec < Path.stroke.getTotalLength()) {
                var currentPoint = Path.stroke.getPointAtLength(Path.sec);
                var c = Path.paper.circle(currentPoint.x, currentPoint.y, 3).attr({ "stroke-width": 1, "stroke": "white", "fill": "red" });
                //闪烁圆点
                var spin = function () {
                    if (c && c.animate) {
                        c.attr('opacity', 0).animate({ opacity: 1.0 }, 500, function () {
                            if (c && c.animate) {
                                c.animate({ opacity: 0 }, 5000, spin);
                            }
                        })
                    }
                }
                spin();
                Path.sec += 24;
            } else {
                clearInterval(spinTimer);
                if(typeof callback=='function')
                    callback();
            }
        }, 150);

    };

    window.Path = {};
    Path.drawPath = drawPath;
    Path.buildPath= buildPath;
    Path.sec=sec;
    Path.stroke=stroke;

})(window);
