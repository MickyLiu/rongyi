(function() {

        /*!
             image slider support auto slide and finger slide
        */

	      var imageSlidey=function(con,slides,option){

                var _init=function(c,s,o){
                    this.current = 0;
                    this.container = c;
                    this.slides = s;
                    var s_one = slides.eq(0);
                    s_one.addClass('active');
                    this.clientWidth = s_one.width();
                    this.option = o;
                    this.auto = true;

                    this.onswipeleft();
                    this.onswiperight();
                    this.onmovestart();
                    this.onmove();
                    this.onmoveend();
                    this.setDots();
                    this.autoSlide();
                }

   
             this.autoSlide=function(){
                 var _this=this;
                var slideInterval=setInterval(function(){
                           if(_this.auto){
                              _this.autoSwipeLeft();
                         }

                },_this.option.dur); 

               this.slideInterval = slideInterval; 
                  
             }

            this.autoSwipeLeft=function(){
                     var slides=this.slides;
                     var _this=this;
                     slides.eq(_this.current).removeClass('active');

                     if (this.current === slides.length - 1) {
                           _this.current=0;
                     }
                     else
                     {
                        _this.current++;
                     }
                     
                     slides.eq(_this.current).addClass('active');

                     if(_this.dots)
                     {
                       var dots = _this.dots.find('li');
                       dots.removeClass('active');
                       dots.eq(_this.current).addClass('active');
                     }
            }

            this.pause=function(){

                this.auto = false;
            }

            this.resume=function(){
                this.auto = true;
            }

            this.stop=function(){

                 clearInterval(this.slideInterval);
            }

            this.setDots = function(){
                   if(this.slides.length>1)
                   {
                       var dotString = '<ol class="dots" style="z-index:1000;"></ol>';
                       this.container.append(dotString);
                       var dots = $('.dots');
                        for (var i = 0; i < this.slides.length ; i++) {
                            if(i==0)
                            {
                               dots.append('<li class="dot active"></li>');
                            }
                            else
                            {
                               dots.append('<li class="dot"></li>');
                            }
                       }
                   }

                   this.dots = $('.dots');
            }

            this.onswipeleft=function(cb){ 
                  var _this=this; 
                  var slides=_this.slides;
                  this.slides.on('swipeleft', function(e) {  
                     if (_this.current === slides.length - 1) {
                           return;
                     }
                     slides.eq(_this.current).removeClass('active');
                     _this.current++;
                     slides.eq(_this.current).addClass('active');

                     if(_this.dots)
                     {
                       var dots = _this.dots.find('li');
                       dots.removeClass('active');
                       dots.eq(_this.current).addClass('active');
                     }

                      if(typeof cb=='function')
                      {
                        cb();
                      }
                  });
            }

            this.onswiperight=function(cb){
                 var _this=this; 
                 var slides=_this.slides;
                 this.slides.on('swiperight', function(e) {
                   if (_this.current === 0) { return; }
                   slides.eq(_this.current).removeClass('active');
                   _this.current--;
                   slides.eq(_this.current).addClass('active');

                    if(_this.dots)
                    {
                       var dots = _this.dots.find('li');
                       dots.removeClass('active');
                       dots.eq(_this.current).addClass('active');
                    }

                   if(typeof cb=='function')
                   {
                      cb();
                   }
                });
            }

            this.onmovestart=function(cb){

               var _this=this;

               this.slides.on('movestart', function(e) {
                 // stop auto slide
                   _this.stop();

                // If the movestart heads off in a upwards or downwards
                // direction, prevent it so that the browser scrolls normally.
                if ((e.distX > e.distY && e.distX < -e.distY) ||
                    (e.distX < e.distY && e.distX > -e.distY)) {
                  e.preventDefault();
                  return;
                }
                // To allow the slide to keep step with the finger,
                // temporarily disable transitions.
                _this.slides.addClass('notransition');

                if(typeof cb=='function')
                {
                   cb();
                }

              });
           }

           this.onmove=function(cb){
                    var _this=this;
                    var slides=_this.slides;
                    this.slides.on('move',function(e){ 
                         var i=_this.current;
                         var width = _this.clientWidth;
                         var left = 100 * e.distX / width;
                        // Move slides with the finger
                        if (e.distX < 0) {
                          if (slides[i+1]) {
                            slides[i].style.left = left + '%';
                            slides[i+1].style.left = (left+100)+'%';
                          }
                          else {
                            slides[i].style.left = left/4 + '%';
                          }
                        }
                        if (e.distX > 0) {
                          if (slides[i-1]) {
                            slides[i].style.left = left + '%';
                            slides[i-1].style.left = (left-100)+'%';
                          }
                          else {
                            slides[i].style.left = left/5 + '%';
                          }
                        }

                        if(typeof cb=='function')
                        {
                           cb();
                        }

                    });        
           }

           this.onmoveend=function(cb){

              var _this=this;
              var slides=_this.slides;
              this.slides.on('moveend', function(e) {
                // go on auto slide
                  _this.autoSlide(); 

                  var i=_this.current;
                   _this.slides.removeClass('notransition');
                  slides[i].style.left = '';
            
                  if (slides[i+1]) {
                    slides[i+1].style.left = '';
                  }
                  if (slides[i-1]) {
                    slides[i-1].style.left = '';
                  }

                  if(typeof cb=='function')
                  {
                     cb();
                  }
            });

          }

         _init.apply(this,arguments);

         return this;

    }

    Utils.imageSlidey = imageSlidey;
    
})();