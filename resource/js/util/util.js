(function() {

	var isFunction = function(fn) {
		return Object.prototype.toString.call(fn).indexOf('Function') != -1;
	}


    
	var Utils = window.Utils || {};

	Utils.isFunction = isFunction;

	Utils.countToUserLeave = function(){

		var startCount = function() {

          var timeSpan= window.pageNavigator.currentPage>0?_Const.slientTimespan:_Const.adOption.option.dur;
  	     	return setTimeout(function(){
               	       console.log('user leaved!');
               	       var page= window.pageNavigator.currentPage;
                       if(page>0)
                       {
                           // 其他页长时无人操作
                           //location.href= "#/";
                          $(".r-page").removeClass("openpage").addClass("closepage");
                          Utils.PrefixedEvent($(".r-page"), "AnimationEnd", function() {
                            App.Router.router.transitionTo('index');
                          });
                       }
                       else if(page==0)
                       {
                           // 首页长时无人操作
                            Utils.jumpFromIndex();
                            Utils.PrefixedEvent($(".wrap-main"), "AnimationEnd", function() {
                                App.Router.router.transitionTo('ad');
                            });
                       }
                       else if(page<0)
                       {
                              // 广告页
                       }
                       else
                       {

                       }
               },timeSpan);
  	     }

        Utils.countToUserLeave.startCount = startCount;
  	    var pageNavigator = window.pageNavigator || {timeoutId:0,currentPage:0};
  	    window.pageNavigator = pageNavigator;
        //window.pageNavigator.timeoutId = startCount();
        
        $(document).bind('click',function(){
               console.log('user touched!');
               if(window.pageNavigator.currentPage>=0)
               {
                   clearTimeout(window.pageNavigator.timeoutId);
                   window.pageNavigator.timeoutId= startCount();
               }
        });
	}

  Utils.adRotation= function(ads,con,option){

        var _init = function(a,c,o){
               this.option = o; 
               this.container = c;
               this.ads = a;
               this.adIndex=-1;
         } 

     this.nextAd=function(){
             this.adIndex++;
             if(this.adIndex>=this.ads.length){
                this.adIndex=0;
             }
             
             var ad= this.ads[this.adIndex];
             var $con= $(this.container);
             $con.html('');
             for(var index=0;index<ad.adSpaces.length;index++)
             {
                 var item=ad.adSpaces[index];
                 if(item.type=='img')
                 {
                   $con.append('<img src="'+ item.src +'"/>');
                 }
             }
     }

     this.prevAd=function(){
             this.adIndex--;
             if(this.adIndex<0){
                this.adIndex=this.ads.length-1;
             }
             var ad= this.ads[this.adIndex];
             var $con= $(this.container);
             $con.html('');
             for(var index=0;index<ad.adSpaces.length;index++)
             {
                 var item=ad.adSpaces[index];
                 if(item.type=='img')
                 { 
                   $con.append('<img src="'+ item.src +'"/>');
                 }
             }
     }

     this.rotationToNext=function(timeSpan,route){
              var _this=this; 
              this.nextAd();
              _this.container.addClass('bounceIn');
              var rid=setTimeout(function(){
                    _this.container.css({"opacity":"1"}).removeClass("bounceIn").addClass("beforeOpenPage");
                    Utils.PrefixedEvent(_this.container, "AnimationEnd", function() {
                                App.Router.router.transitionTo(route);
                   });
              },timeSpan);
     }

     _init.apply(this,arguments);
  }


  Utils.initAd = function(){

        $.getJSON(_Const.url.getAdvertisement).then(function(data) {
            console.log('ad data:'+ data);

            var ads=[];
            for(var i=0;i<data.length;i++)
            {
                ads.push({
                adSpaces:[{
                      src:data[i].src,
                      type:'img'
                  }]
               });
            }

            if(!App.adRotation)
            {
                App.adRotation = new Utils.adRotation(ads,null,_Const.adOption.option);
            }
     });

  }

  Utils.countToUserLeave();
  Utils.initAd();
	window.Utils = Utils;
    
})();