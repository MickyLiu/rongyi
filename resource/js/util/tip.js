(function(window) {
	
	var com = {};
	com.easyGuide = {};
	com.easyGuide.Map = {};

	/**
	 * 地图品牌动态标签
	 *
	 * @author hao.li <lihao@rongyi.com>
	 *
	 * @date 2013.09.23
	 */
	 com.easyGuide.Map.Tip = function (model, Runtime) {
	    var _this = this;
	    var _init = function (model, Runtime) {
	        this.Runtime = Runtime
	        this.logo = {
	            code: "",
	            x: 0,
	            y: 0,
	            path: "",
	            width: model.width,
	            height: model.height
	        }
	        this.frame = new com.easyGuide.Map.Tip.Frame(model, Runtime);
	    }

	    this.show = function (x, y, path, code) {
	        //更新本地数据
	        this.logo.code = code;
	        this.logo.path = path;
	        this.logo.x = x;
	        this.logo.y = y;
	        this.frame.update(x, y, path, code);
	        this.Runtime.updateTimerStatus();
	    }

        this.scale=function(params){
            this.frame.scale(params);
        }
	    this.updateTimer = function () {
	        this.frame.updateTimer();
	    }

	    this.hide = function () {
	        this.frame.hide();
	    }
	    _init.apply(this, arguments);
	}

	/**
	 * 动态标签载体
	 *
	 * @author hao.li <lihao@rongyi.com>
	 *
	 * @date 2013.09.23
	 */
	 com.easyGuide.Map.Tip.Frame = function (model, Runtime) {
	    var _this = this, is_label_visible = false, side = "circle";
	    var _init = function (model, Runtime) {
	        this.Runtime = Runtime;
	        this.paper = model.paper;
	        this.label = model.label = model.paper.set();
	        this.view = _createView.call(this, model);
	        this.area = new com.easyGuide.Map.Tip.BTArea(this, model, Runtime);
	    }

	    var _createView = function (model) {
	        //var image = this.paper.image(model.path, -model.width / 2, -model.height / 2, model.width, model.height);
	        //image.attr({ "stroke": "#333", "stroke-width": 1, cursor: "pointer" });

	        var circle = this.paper.circle(0, 0, model.width / 2).attr({ stroke: "none" });
	        var uuid = Raphael.createUUID();
	        var pattern = document.createElementNS("http://www.w3.org/2000/svg", "pattern");
	        pattern.setAttribute("id", uuid);
	        pattern.setAttribute("x", 0);
	        pattern.setAttribute("y", 0);
	        pattern.setAttribute("height", 1);
	        pattern.setAttribute("width", 1);
	        pattern.setAttribute("patternContentUnits", "objectBoundingBox");

	        this.backgroundImage = this.paper.image(model.path, 0, 0, 1, 1);
	        $(this.backgroundImage.node).appendTo(pattern);
	        $(pattern).appendTo(this.paper.defs);

	        $(circle.node).attr("fill", "url(#" + pattern.id + ")");
	        circle.click(function () { _this.area.extendMenu() });
            if(model.scale){
                circle.attr({transform:["s",model.scale]});
                this.backgroundImage.attr({transform:["s",model.scale]});
            }

	        this.label.push(circle);
	        this.label.hide();

	        return this.popup(0, 0, this.label).hide();
	    }


	    this.update = function (x, y, path, code) {
	        var ppp = this.popup(x, y, this.label, side, 1),
	        anim = Raphael.animation({
	            transform: ["t", ppp.cx, ppp.cy]
	        }, 200 * is_label_visible);
	        this.view.show().stop().animate(anim);
	        this.backgroundImage.attr("src", path);
	        this.label[0].show().stop().animateWith(this.view, anim, { transform: ["t", ppp.cx, ppp.cy] });
	        this.area.update(ppp.cx, ppp.cy, { x: x, y: y, code: code, path: path });

	    }

	    this.popup = function (X, Y, set, rec) {
	        var bb = set.getBBox(),
	        w = Math.round(bb.width),
	        h = Math.round(bb.height),
	        r = w / 2,
	        x = Math.round(bb.x),
	        y = Math.round(bb.y),
	        cx = X,
	        cy = Y - r,
	        gap = model.scale?14*model.scale:14,
	        len = model.scale?7*model.scale:7,
	        ly = Y + r + gap;
	        out = this.paper.set();
	        circle = this.paper.circle(X, Y, r).attr(LOCAL_DATA.CLASS.TIP_FRAME);
	        arrow = this.paper.path("M" + X + "," + ly + "L" + (X - len) + "," + (ly - gap) + "H" + len + "L" + X + "," + ly + "Z").attr(LOCAL_DATA.CLASS.TIP_ARROW);
	        out.push(circle);
	        out.push(arrow);
	        out.insertBefore(set);
	        out.translate(cx, cy - gap);
	        if (rec) {
	            out.remove();
	            return { cx: cx, cy: cy - gap, r: r }
	        }

	        return out;

	    }

	    this.updateTimer = function () {
	        this.area.updateTimer();
	    }

	    this.hide = function () {

	        this.area.destory();

	        this.label.hide();

	        this.view.hide();
	    }

	    _init.apply(this, arguments);
	}

	/**
	 * 动态标签按钮域
	 *
	 * @author hao.li <lihao@rongyi.com>
	 *
	 * @date 2013.09.23
	 */
	 com.easyGuide.Map.Tip.BTArea = function (parent, model, Runtime) {
	    var _this = this, isClose = true, isHidden = true,
	    rad = Math.PI / 180, startAngle = 0, endAngle = 45,
	    cx = 0, cy = 0, r = 41, ms = 800, timeOutHandler, timerOuter = 6, currentOuter = 0,
	    escalator = _Const.imageUrl.escalator, 
	    elevator = _Const.imageUrl.elevator, 
	    detailSectorIcon = _Const.imageUrl.detail, 
	    pathSectorIcon = _Const.imageUrl.path;

	    var _init = function (parent, model, Runtime) {
	        this.Runtime = Runtime;
	        this.parent = parent;
	        this.paper = model.paper;
	        this.logo = model.logo;
	        this.sectorSet = this.paper.set();
	        this.escalatorSet = this.paper.set();
	        this.elevatorSet = this.paper.set();
	        this.data = {};
	        this.tipType = model.tipType ? model.tipType : this.Runtime.tipModel.model;

	        if (model.sectorType) {
	            this.sectorType = model.sectorType;
	        } else if (this.Runtime.tipModel) {
	            this.sectorType = this.Runtime.tipModel.sector;
	        } else {
	            this.sectorType = LOCAL_DATA.CONTENTTYPE.TIP_SECTOR_TEXT;
	        }

	        //this.sectorType = model.sectorType ? model.sectorType : this.Runtime.tipModel.sector;

	        r = model.width / 2
	        cx = model.x;
	        cy = model.y;

	        _initBTSector.call(this);
	    }


	    var _initBTSector = function () {

	        switch (this.tipType) {
	            case LOCAL_DATA.CONTENTTYPE.TIP_TYPE_ALL:

	            this.clearButton();
	            this.sectorSet.remove();
	            this.sectorSet = this.paper.set();
	            var dSector = _sector.call(this, cx, cy, r, startAngle, endAngle, LOCAL_DATA.CLASS.DETAIL_SECTOR), dText;
	            var nSector = _sector.call(this, cx, cy, r, endAngle, endAngle * 2, LOCAL_DATA.CLASS.NAVIGATOR_SECTOR), nText;
	            if (this.sectorType && this.sectorType == LOCAL_DATA.CONTENTTYPE.TIP_SECTOR_IMG) {
	                dText = this.paper.image(detailSectorIcon, cx + r * 4 / 7, cy - r * 3 / 7, 13, 13);
	                nText = this.paper.image(pathSectorIcon, cx + r / 7, cy - 5 * r / 6, 13, 13);
	            } else {
	                dText = this.paper.text(cx + r * 0.68, cy - r * 0.25, "查 看\n详 情").attr(LOCAL_DATA.CLASS.DETAIL_TEXT);
	                nText = this.paper.text(cx + r * 0.3, cy - r * 0.69, "怎 么\n走 ？").attr(LOCAL_DATA.CLASS.DETAIL_TEXT);
	            }

	            this.sectorSet.push(dSector);
	            this.sectorSet.push(dText);
	            this.sectorSet.push(nSector);
	            this.sectorSet.push(nText);
	            this.sectorSet.insertBefore(this.parent.view);

	            /*this.sectorSet[0].click(function () {
	                console.log('in brandLoader 1');
	                //查看详情
	                _this.Runtime.brandLoader.goDetailPage(_this.data.code);
	                $("#ReturnType").val("floor");
	                $("#ReturnFloor").val(_this.Runtime.index);
	                $("#ReturnBrand").val(_this.data.code);
	            });

	            this.sectorSet[1].click(function () {
	                console.log('in brandLoader 2');
	                return false;
	                //查看详情
	                _this.Runtime.brandLoader.goDetailPage(_this.data.code);
	                $("#ReturnType").val("floor");
	                $("#ReturnFloor").val(_this.Runtime.index);
	                $("#ReturnBrand").val(_this.data.code);

	             });*/

	            this.sectorSet[2].click(function () {
	                //展开菜单
	                _this.extendButton();
	            });
	            this.sectorSet[3].click(function () {
	                //展开菜单
	                _this.extendButton();
	            });

	            break

	        case LOCAL_DATA.CONTENTTYPE.TIP_TYPE_NONAVI:

	            this.clearButton();
	            this.sectorSet.remove();
	            this.sectorSet = this.paper.set();
	            var dSector = _sector.call(this, cx, cy, r, startAngle, endAngle, LOCAL_DATA.CLASS.DETAIL_SECTOR), dText;
	            if (this.sectorType && this.sectorType == LOCAL_DATA.CONTENTTYPE.TIP_SECTOR_IMG) {
	                dText = this.paper.image(detailSectorIcon, cx + r * 4 / 7, cy - r * 3 / 7, 13, 13);
	            } else {
	                dText = this.paper.text(cx + r * 0.68, cy - r * 0.25, "查 看\n详 情").attr(LOCAL_DATA.CLASS.DETAIL_TEXT);
	            }
	            this.sectorSet.push(dSector);
	            this.sectorSet.push(dText);
	            this.sectorSet.insertBefore(this.parent.view);
	            /*this.sectorSet[0].click(function () {
	                console.log('in brandLoader 3');
	                //查看详情
	                _this.Runtime.brandLoader.goDetailPage(_this.data.code);
	                $("#ReturnType").val("floor");
	                $("#ReturnFloor").val(_this.Runtime.index);
	                $("#ReturnBrand").val(_this.data.code);
	            });
	            this.sectorSet[1].click(function () {
	                console.log('in brandLoader 4');
	                //查看详情
	                _this.Runtime.brandLoader.goDetailPage(_this.data.code);
	                $("#ReturnType").val("floor");
	                $("#ReturnFloor").val(_this.Runtime.index);
	                $("#ReturnBrand").val(_this.data.code);
	            });*/

	        break
	        case LOCAL_DATA.CONTENTTYPE.TIP_TYPE_NODETAIL:
	            this.clearButton();
	            this.sectorSet.remove();
	            this.sectorSet = this.paper.set();
	            var nSector = _sector.call(this, cx, cy, r, endAngle, endAngle * 2, LOCAL_DATA.CLASS.NAVIGATOR_SECTOR), nText;
	            if (this.sectorType && this.sectorType == LOCAL_DATA.CONTENTTYPE.TIP_SECTOR_IMG) {
	                nText = this.paper.image(pathSectorIcon, cx + r / 7, cy - 5 * r / 6, 13, 13);
	            } else {
	                nText = this.paper.text(cx + r * 0.3, cy - r * 0.69, "怎 么\n走 ？").attr(LOCAL_DATA.CLASS.DETAIL_TEXT);
	            }

	            this.sectorSet.push(nSector);
	            this.sectorSet.push(nText);
	            this.sectorSet.insertBefore(this.parent.view);
	            this.sectorSet[0].click(function () {
	                //展开菜单
	                _this.extendButton();
	            });
	            this.sectorSet[1].click(function () {
	                //展开菜单
	                _this.extendButton();
	            });

	        break
	        case LOCAL_DATA.CONTENTTYPE.TIP_TYPE_SIMPLE:

	        break
	    }

	    isClose = true;
	}

	var _initBTCircle = function () {

	    this.clearButton();
	    var ccx = cx + r * 2.3 - r / 2;

	        //扶梯按钮区域
	        var BTEscalator = this.paper.circle(ccx, cy, r / 2).attr(LOCAL_DATA.CLASS.NAVIGATOR_BUTTON),
	        ICONEscalator = this.paper.image(escalator, ccx - r / 2, cy - r / 2, r, r).attr({ cursor: "pointer" });
	        ICONEscalator.click(function () {

	            if (!_this.Runtime.mapBuilder) {//从详情页发来的请求
	            	console.log('line 323');
	                var detailData = _getDetailData();
	                detailData.byDetail = true,
	                detailData.naviType = LOCAL_DATA.CONTENTTYPE.NAVIGATE_BY_ESCALATOR;

	                com.easyGuide.Navigator.findPathByDetail(detailData);
	            } else {
	                //导航
	                _this.data.index = _this.Runtime.index;
	                _this.Runtime.navigate(_this.data, LOCAL_DATA.CONTENTTYPE.NAVIGATE_BY_ESCALATOR);
	            }
	        });
	        this.escalatorSet.push(BTEscalator);
	        this.escalatorSet.push(ICONEscalator);
	        this.escalatorSet.transform("r-65.5," + cx + "," + cy);

	        //电梯按钮区域
	        var BTElevator = this.paper.circle(ccx, cy, r / 2).attr(LOCAL_DATA.CLASS.NAVIGATOR_BUTTON),
	        ICONElevator = this.paper.image(elevator, ccx - r / 2, cy - r / 2, r, r).attr({ cursor: "pointer" });
	        ICONElevator.click(function () {

	            if (!_this.Runtime.mapBuilder) {//从详情页发来的请求
	            	console.log('line 345');
	                var detailData = _getDetailData();
	                detailData.byDetail = true,
	                detailData.naviType = LOCAL_DATA.CONTENTTYPE.NAVIGATE_BY_ELEVATOR;

	                com.easyGuide.Navigator.findPathByDetail(detailData);
	            } else {
	                //导航
	                _this.updateTimerStatus();
	                _this.data.index = _this.Runtime.index;
	                _this.Runtime.navigate(_this.data, LOCAL_DATA.CONTENTTYPE.NAVIGATE_BY_ELEVATOR);
	            }

	        });
	        this.elevatorSet.push(BTElevator);
	        this.elevatorSet.push(ICONElevator);

	        var raid = "r-21.5,";
	        if (this.tipType == LOCAL_DATA.CONTENTTYPE.TIP_TYPE_NODETAIL) {
	            raid = "r-65.5,"
	        }

	        this.elevatorSet.transform(raid + cx + "," + cy);

	        this.elevatorSet.insertBefore(this.sectorSet);
	        this.escalatorSet.insertBefore(this.sectorSet);
	    }


	    var _start = function () {
	        this.stop();
	        timeOutHandler = setInterval(function () {
	            currentOuter++;
	            if (currentOuter >= 6) {
	                _this.close();
	            }
	        }, 1000)
	    }

	    this.stop = function () {
	        clearInterval(timeOutHandler);
	        this.updateTimerStatus();
	    }

	    this.updateTimer = function () {
	        currentOuter = 0;
	    }

	    this.updateTimerStatus = function () {
	        currentOuter = 0;
	        this.Runtime.updateTimerStatus();
	    }


	    this.update = function (x, y, data) {

	        cx = x;

	        cy = y;

	        _initBTSector.call(this);

	        this.data = data;

	        this.extendMenu();
	    }

	    this.extendMenu = function () {
	        if (isClose) {
	            anim = Raphael.animation({
	                transform: "s2.3 2.3 " + cx + " " + cy
	            }, ms, "elastic", function () {
	                isClose = false;
	                if (_this.tipType == LOCAL_DATA.CONTENTTYPE.TIP_TYPE_ALL || _this.tipType == LOCAL_DATA.CONTENTTYPE.TIP_TYPE_NODETAIL) {
	                    _initBTCircle.call(_this);
	                }
	            });

	            this.sectorSet.animate(anim);

	            _start.call(this);

	        } else {
	            this.close();
	        }
	    }

	    this.close = function (callback) {
	        this.stop();
	        if (!isHidden) {
	            this.hiddenButton(function () {
	                _this.clearButton();
	                _this.sectorSet.stop().animate({ transform: "s1 1 " + cx + " " + cy }, ms / 2, "elastic", function () {
	                    isClose = true;
	                });
	                if (typeof callback == "function") callback();
	            });
	        } else {
	            this.clearButton();
	            this.sectorSet.stop().animate({ transform: "s1 1 " + cx + " " + cy }, ms / 2, "elastic", function () {
	                isClose = true;
	            });
	            if (typeof callback == "function") callback();
	        }

	    }

	    var _getDetailData = function () {
	    	console.log('in _getDetailData');
	        return {
	            floorCode: $("#brandFloorCode").val(),
	            code: $("#brandCode").val(),
	            floor: $("#brandFloorTitle").val(),
	            x: $("#logoX").val(),
	            y: $("#logoY").val(),
	            path: $("#brandLogo").val()
	        }
	    }

	    this.extendButton = function () {
	        if (!this.Runtime.mapBuilder) {  //从详情页发来的请求
	            var detailData = _getDetailData(); 
	            console.log('line 467');
	            detailData.byDetail = true;
	            if (model.location) {
	                if (model.location.floor != detailData.floor) {//跨层
	                    if (isHidden) {
	                        anim = Raphael.animation({
	                            transform: "r-121.5 " + cx + " " + cy
	                        }, ms / 2, "ease-in-out", function () {
	                            isHidden = false;
	                        });
	                        this.escalatorSet.stop().animateWith(this.elevatorSet, anim, { transform: "r-165.5 " + cx + " " + cy }, ms / 2, "ease-in-out");

	                        this.elevatorSet.stop().animate(anim);

	                    } else {
	                        this.hiddenButton();
	                    }
	                } else {//同层

	                    com.easyGuide.Navigator.findPathByDetail(detailData);
	                }
	            }
	        } else {//从楼层页发来的请求

	            this.updateTimerStatus();

	            if (this.Runtime.location.index == this.Runtime.index) {
	                //同层直接开始导航
	                this.data.index = this.Runtime.index;
	                this.Runtime.navigate(this.data, LOCAL_DATA.CONTENTTYPE.NAVIGATE_BY_ELEVATOR);

	            } else {
	                if (isHidden) {
	                    anim = Raphael.animation({
	                        transform: "r-121.5 " + cx + " " + cy
	                    }, ms / 2, "ease-in-out", function () {
	                        isHidden = false;
	                    });
	                    this.escalatorSet.stop().animateWith(this.elevatorSet, anim, { transform: "r-165.5 " + cx + " " + cy }, ms / 2, "ease-in-out");

	                    this.elevatorSet.stop().animate(anim);

	                } else {
	                    this.hiddenButton();
	                }
	            }
	        }
	    }

	    this.hiddenButton = function (callbcak) {
	        anim = Raphael.animation({
	            transform: "r-65.5 " + cx + " " + cy
	        }, ms / 2, "ease-in-out", function () {
	            isHidden = true;
	            if (typeof callbcak == "function") callbcak();
	        });

	        var raid = "r-21.5,";
	        if (this.tipType == LOCAL_DATA.CONTENTTYPE.TIP_TYPE_NODETAIL) {

	            raid = "r-65.5,"
	        }
	        this.escalatorSet.stop().animateWith(this.elevatorSet, anim, { transform: raid + cx + " " + cy }, ms / 2, "ease-in-out");

	        this.elevatorSet.stop().animate(anim);
	    }

	    this.clearButton = function () {
	        this.escalatorSet.remove();
	        this.escalatorSet = this.paper.set();

	        this.elevatorSet.remove();
	        this.elevatorSet = this.paper.set();

	        isHidden = true;
	    }

	    this.destory = function () {
	        this.clearButton();
	        this.sectorSet.remove();
	        this.sectorSet = this.paper.set();
	        this.stop();
	        isClose = true;
	    }

	    var _sector = function (cx, cy, r, startAngle, endAngle, params) {
	        var x1 = cx + r * Math.cos(-startAngle * rad),
	        x2 = cx + r * Math.cos(-endAngle * rad),
	        y1 = cy + r * Math.sin(-startAngle * rad),
	        y2 = cy + r * Math.sin(-endAngle * rad);
	        return this.paper.path(["M", cx, cy, "L", x1, y1, "A", r, r, 0, +(endAngle - startAngle > 180), 0, x2, y2, "z"]).attr(params);
	    }

	    _init.apply(this, arguments);
	};

	var LOCAL_DATA = {
	    MODEL: {
	        LOCATION_MARK: "M16,3.5c-4.142,0-7.5,3.358-7.5,7.5c0,4.143,7.5,18.121,7.5,18.121S23.5,15.143,23.5,11C23.5,6.858,20.143,3.5,16,3.5z M16,14.584c-1.979,0-3.584-1.604-3.584-3.584S14.021,7.416,16,7.416S19.584,9.021,19.584,11S17.979,14.584,16,14.584z"
	    },
	    CLASS: {
	        PATH_STROKE: { "stroke-width": 1, "stroke": "#060", "stroke-opacity": 0 },
	        PATH_CIRCLE_POINT: { "stroke-width": 1, "stroke": "red", "fill": "red" },
	        TIP_ARROW: { fill: "#545868", stroke: "none" },
	        TIP_FRAME: { fill: "#000", stroke: "#545868", "stroke-width": 7, "fill-opacity": .7 },
	        LOCATION_MARK: { 'stroke-width': 1, 'stroke': '#DC143C', 'fill': '#FF0000', width: 16, height: 27 },
	        LOCATION_MARK_TEXT: { 'fill': '#FF0000', 'font-family': '微软雅黑', 'font-size': 12, 'font-weight': '800' },
	        DETAIL_SECTOR: { fill: "#42404f", stroke: "#fff", "stroke-width": 1, cursor: "pointer" },
	        NAVIGATOR_SECTOR: { fill: "#e6004b", stroke: "#fff", "stroke-width": 1 },
	        NAVIGATOR_BUTTON: { fill: "#e6004b", stroke: "#fff", "stroke-width": 1, cursor: "pointer" },
	        DETAIL_TEXT: { fill: "#fff", font: "7px 微软雅黑", stroke: "#fff", "stroke-width": 0.2, cursor: "pointer" }
	    },
	    CONTENTTYPE: {
	        NAVIGATE_BY_ESCALATOR: -1,
	        NAVIGATE_BY_ELEVATOR: 1,
	        TIP_TYPE_ALL: -1,
	        TIP_TYPE_NONAVI: 1,
	        TIP_TYPE_NODETAIL: 2,
	        TIP_TYPE_SIMPLE: 3,
	        TIP_SECTOR_IMG: 1,
	        TIP_SECTOR_TEXT: 2
	    }
	};

	window.com = com;
	window.LOCAL_DATA = LOCAL_DATA;

})(window);
