/*
 *
 * 页面之间跳转动画效果
 *
 */
(function(window, $) {

	//初始动画
   	var initAnimate = function(){
   		$(".top_wrap").css({"animation-delay":"0s"}).addClass("bounceIn");
	    $(".nav_wrap").css({"animation-delay":".2s"}).addClass("bounceIn");
	    $(".map_wrap").css({"animation-delay":".4s"}).addClass("bounceIn");
   	}
    

    //二级页面返回动画
    var contentBackAnimate = function(){
    	$(".top_wrap").css({"opacity":"0","animation-delay":"0s"}).removeClass("beforeOpenPage").addClass("initAnimate");
    	$(".nav_wrap").css({"opacity":"0","animation-delay":".2s"}).removeClass("beforeOpenPage").addClass("initAnimate");
    	$(".map_wrap").css({"opacity":"0","animation-delay":".4s"}).removeClass("beforeOpenPage").addClass("initAnimate");
    }

    //点击跳转到别的页面的动画效果
    var jumpFromIndex = function(sRoute) {
    	$(".top_wrap").css({"opacity":"1"}).removeClass("bounceIn").removeClass("initAnimate").addClass("beforeOpenPage");
    	$(".nav_wrap").css({"opacity":"1"}).removeClass("bounceIn").removeClass("initAnimate").addClass("beforeOpenPage");
        $(".map_wrap").css({"opacity":"1"}).removeClass("bounceIn").removeClass("initAnimate").addClass("beforeOpenPage");

        //跳转到内容页面
        // $(".top_wrap").bind("animationend",function(e){
        // 	self.get('controller').transitionToRoute('content');
        // });
        
    };

    var pfx = ["webkit",""];
    var PrefixedEvent = function(element, type, callback){
        for(var p = 0; p < pfx.length; p++){
            if(!pfx[p]){
                type = type.toLowerCase();
            }
            element.bind(pfx[p]+type,function(e){
                callback();
            });
        }
    };


    //点击跳回首页的动画效果
    var jumpToIndex = function() {
    	//动画写在这里
		$(".r-page").removeClass("openpage").addClass("closepage");
		//动画结束之后跳转
		// $(".r-page").bind("animationend",function(e){
  //       	self.get('controller').send('gotoIndex');
  //       });
    }

    window.Utils = window.Utils || {};
    window.Utils.initAnimate = initAnimate;
    window.Utils.contentBackAnimate = contentBackAnimate;
    window.Utils.jumpFromIndex = jumpFromIndex;
    window.Utils.jumpToIndex = jumpToIndex;
    window.Utils.PrefixedEvent = PrefixedEvent;

})(window, jQuery);