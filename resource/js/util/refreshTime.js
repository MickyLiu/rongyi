/*
 *
 * 每一分钟刷新时间功能
 *
 */
(function(window) {

	var refreshTime = function() {
		var currentDate = new Date();
		var year = currentDate.getFullYear();
		var month = currentDate.getMonth() + 1;
		var date = currentDate.getDate();
		var hour = currentDate.getHours();
		var minute = currentDate.getMinutes();
		minute = minute < 10 ? '0' + minute : minute;
		var am = hour < 12 ? 'AM' : 'PM';
		var day = currentDate.getDay();
		day = _Const['text']['weekdays'][day];

		

		sDate = year + "年" + month + "月" + date + "日  " + day + "";
		sTime = hour + ":" + minute;
		sAM = am;

		return {
			sDate: sDate,
			sTime: sTime,
			sAM: sAM
		};

	};

	window.Utils = window.Utils || {};
	window.Utils.refreshTime = refreshTime;

})(window);