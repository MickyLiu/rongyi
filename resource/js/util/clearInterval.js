/*
 *
 * 清除页面的所有计时器相关的绑定
 *
 */
 (function(window) {

 	var queue = [];	//绑定了的interval

	var addInterval = function(i) {
		queue.push(i);
	};

	//取消所有绑定了的事件
	var clearIntervals = function() {
		for (var i = 0, len = queue.length; i < len; i++) {
			clearInterval(queue.shift());
		}
	};

	window.GC = window.GC || {};

	window.GC.addInterval = addInterval;
	window.GC.clearIntervals = clearIntervals;


 })(window);