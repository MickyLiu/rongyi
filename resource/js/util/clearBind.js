/*
 *
 * 负责所有事件取消绑定回收内存的功能
 *
 */
(function(window) {


	var eles = [];	//绑定了事件的元素

	var addBinding = function(ele) {
		eles.push(ele);
	};

	//取消所有绑定了的事件
	var clearBinding = function() {
		for (var i = 0, len = eles.length; i < len; i++) {
			var ele = eles.shift();
			ele.off ? ele.off() : '';
		}
	};

	window.GC = window.GC || {};

	window.GC.addBinding = addBinding;
	window.GC.clearBinding = clearBinding;


})(window);