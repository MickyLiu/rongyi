/*
 *
 * 画svg图相关功能
 * 点击设施高亮的逻辑也加入了
 *
 */
(function(window, $) {

	var noop = function() {};

	var oFacility = {};
	var oItem = {};
	var currentHighFacility;
	var currentItem;
	var highlightInterval;
	var timeoutInterval;
    var oFacilytys=[];
	window.Facilities = {};

	var bigImageParams = [0, 0, 1028, 680];
	var smallImageParams = [0, 0, 680, 450];
	var iconParams = [82, 82];

	var p = {
		iconParams: iconParams,
		bigImageParams: bigImageParams,
		smallImageParams: smallImageParams
	};


	//使用Raphael库画入一张图片
	var insertImage = function(options) {

		var defaultOptions = {
			params: [0, 0, 1028, 680],	//用Raphael画下方大图需要用到的x, y, width, height参数
			callback: noop
		};

		var options = $.extend({}, defaultOptions, options);

		var imgSrc = options.imgSrc;
		var paper = options.paper;
		var params = options.params;
		var callback = options.callback;

		var paramList = params.concat();
		paramList.unshift(imgSrc);
		var target = paper.image.apply(paper, paramList);
		setTimeout(function() {
			if (Utils.isFunction(callback)) {
				callback();
			}	
		}, 0);
		return target;
	};


	var clearHighFacility = function() {
		if (!currentHighFacility) {
			return false;
		}
		clearInterval(highlightInterval);
		currentHighFacility.common.hide();
		currentItem.high.hide();
		currentItem.common.show();
	};

	var getFacilities = function(name) {
		return oFacility[name];
	};

	var getItems = function(name) {
		return oItem[name];
	};

	var highlightFacility = function(facility) {
		
		clearTimeout(timeoutInterval);
        facility.common.show();

        var spin=function(){
            facility.common.animate({opacity:.3},1000*0.75,function(){
                 facility.common.animate({opacity:1},1000*0.45);
            });
        }
        spin();
		highlightInterval = setInterval(function() {
            spin();
		}, 1000 * 1.2);
		timeoutInterval = setTimeout(function() {
			clearHighFacility();
		}, 1000 * 6);
	}

	var drawMap = function(mapObj, options) {
		try {
			var paper = options.paper;
			var callback = options.callback;
			/******************画背景大图开始*****************/
			var rBg = insertImage({
				imgSrc: mapObj.bg||mapObj[0].map_pic,
				params: options.isBigImage ? bigImageParams : smallImageParams,
				paper: paper
			});
			/******************画背景大图结束*****************/

			
			/******************画公共设施图开始*****************/
			if (options.showFacilities) {
				var facilities = mapObj.facilities;
				for (var i = 0, len = facilities.length; i < len; i++) {
					var facility = facilities[i];
					var facilityName = facility.name;
					oFacility[facilityName] = {};
					oFacility[facilityName].common = insertImage({
						imgSrc: facility.url,
						params: options.isBigImage ? bigImageParams : smallImageParams,
						paper: paper
					});
		            oFacility[facilityName].common.hide();

		            oFacilytys.push(oFacility[facilityName].common);
				}
			}
			/******************画公共设施图结束*****************/



			/******************画侧边按钮开始*****************/
			if (options.showSideButtons) {
				var items = mapObj.items;
				var initY = 70;
				var stepY = 90;
				for (var i = 0, len = items.length; i < len; i++) {
					var item = items[i];
					var itemName = item.name;
					var commonUrl = item.url;
					var highUrl = item.red;
					oItem[itemName] = {};
					oItem[itemName].common = insertImage({
						imgSrc: commonUrl,
						params: [10, initY, 71, 71],
						paper: paper
					});
					oItem[itemName].high = insertImage({
						imgSrc: highUrl,
						params: [10, initY, 71, 71],
						paper: paper
					});
					oItem[itemName].high.hide();
					if (itemName == 'ServicesCenter' || itemName == 'Washroom') {
						window['Facilities'][itemName] = oItem[itemName].common;
					}
					oItem[itemName].common._name_ = itemName;
					initY += stepY;
					oItem[itemName].common.click(function() {
						clearHighFacility();//停止当前正在高亮的公共设施
						var selfName = this._name_;
						currentHighFacility = getFacilities(selfName);
		                for(var i=0;i<oFacilytys.length;i++){
		                    oFacilytys[i].hide();
		                }
						currentItem = getItems(selfName);
						currentItem.common.hide();
						currentItem.high.show();
						highlightFacility(currentHighFacility);
					});
				}
			}

			/******************画侧边按钮结束*****************/

			//画起始位置的提示
			if (mapObj.location) {
				var loc = mapObj.location;
		        var _width=loc.width,
		            _height=loc.height,
		            lx=options.isBigImage?(_Const.startXY.x/0.66-_width/2):(_Const.startXY.x-_width/2),
		            ly=options.isBigImage?(_Const.startXY.y/0.66-_height/2-loc.arrow):(_Const.startXY.y-_height/2-loc.arrow);
		        var mark=insertImage({
				        imgSrc: loc.url,
				        params: [lx, ly, _width, _height],
				        paper: paper
				    });
		        mark.attr({ transform: ["t",0 , 0] });
		        window.GC.addInterval(setInterval(function () {
		            mark.animate({ transform: ["t", 0, - 10] }, 490, function () {
		                mark.animate({ transform: ["t", 0, 0] }, 340);
		            })
		        }, 840));
			}
	        if (Utils.isFunction(callback)) {
	        	callback();
	        }
		} catch (e) {
			console.log('in drawMap');
			console.log(e);
		}
	};

	//画一套地图
	//参数需要读取一套地图的url，以及可选参数（包括是否需要公共设施，是否从本地读取，回调函数）
	//url是必要参数
	//返回的地图对象应该是提供一套完整的图片路径
	var loadMap = function(options) {

		var defaultOptions = {
			showFacilities: false,//是否需要显示公共设施
			loadFromLocal: false,//是否从本地读取地图
			showSideButtons: false,//是否显示旁边的公共设施按钮
			isBigImage: true,		//是否画大图，决定params参数
			callback: noop
		}

		var options = $.extend({}, defaultOptions, options);

		if (options.loadFromLocal) {//从本地读取图片

			$.ajax({
				url: options.url
			}).then(function(data){
				if (typeof data === 'object') {
					drawMap(data, options);
				} else {
					data = eval("(" + data + ")");	
					drawMap(data, options);
				}
			});

		} else {//调用接口读取图片
			$.getJSON(options.url + '?callback=?').then(function(data) {
				drawMap(data, options);
			});
		}
	};



	var MapLoader = window.MapLoader || {};
	MapLoader.loadMap = loadMap;
	MapLoader.insertImage = insertImage;
	MapLoader.p = p;

	window.MapLoader = MapLoader;

})(window, jQuery);
