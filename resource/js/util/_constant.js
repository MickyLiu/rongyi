(function(window) {
	//9zsh3$Q4
	// var _host = 'http://192.168.1.153:3001';
	 //var _host = 'http://192.168.1.89:3000';
	// var _host = 'http://192.168.1.132:3001';
	var _host = 'http://192.168.1.89:3000';
    //var _host = 'http://192.168.1.89:3000';
	var _Const = {
		url: {
			getBottomMapFromLocal: 'resource/js/data/bottomMap.json',
			getStores: _host + '/term_api/shop_list',
			getCategory: _host + '/term_api/category_list',
			getIndexCategory: _host + '/term_api/index_category_list',
			getStaticPic: _host + '/term_api/get_pic_for_pages',
			getBottomMapPic: _host + '/term_api/get_nav_pics',
			getMapShops: _host + '/term_api/map_shops_by_floor',
			getNavBarInfo: _host + '/term_api/get_page_title',
			getTerminal: _host + '/terminals.json',
			setTerminal: _host + '/term_api/set_default_terminal?terminal_id=1',
			getUndergroundParkInfo: _host + '/term_api/reach_undergroung_lot?callback=?&parking_code=',
			getUndergroundLotMap: _host + '/term_api/get_underground_lot_map',
			get3DParkInfo: _host + '/term_api/reach_stereo_lot?callback=?&parking_code=',
            get3DParkInfoMap:'resource/js/data/3dPark.json',
			getAvailableLetters: _host + '/term_api/get_letters_collection?callback=?',
			getActivityDetail: _host + '/term_api/get_activity_detail?callback=?',
			getAdvertisement: _host + '/term_api/get_pic_for_pages?callback=?&title=advertisement',
			serverPrefix: _host + '/system',
			getShopByShopRoom: _host +'/term_api/get_shop_by_shop_room?callback=?&shop_room=',
			//getActivityDetail: _host + '/resource/js/data/activity.json',

			getWeather: _host + '/term_api/get_weather'
		},
		text: {
			weekdays: ['星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六'],
			category: 'category',
			letter: 'letter',
			park_instrunction_3d: '立体车库[area]',
			park_instrunction_underground: '地下车库[area]',
			empty_park_msg: '你的车位号信息为空，请重新查询',
			isFavored: 'isFavored'
		},
		title: {
			member: '会员中心',
			mall_info: '商场介绍',
			floor: '楼层导航',
			brand: '品牌导购',
			park: '找车位'
		},
		path: {
			F1:[] ,
			Park: []
		},
		imageUrl: {
			F1: 'resource/images/floor_tb/path_1F.png',
			elevator: 'resource/images/tool/elevator.png',
			escalator: 'resource/images/tool/escalator.png',
            location:{path:'resource/images/tool/location.png',width:118,height:64,arrow:36},
			path: 'resource/images/tool/path.png',
			detail: 'resource/images/tool/detail.png',
			b1_bg: 'resource/images/floor_tb/b1_bg.png',
			b1_path: 'resource/images/floor_tb/b1_path.png',
			_3dPark: 'resource/images/floor_tb/_3dPark.png',
			address: 'resource/images/floor_tb/address_number.png'
		},
		pages: [{
             url:'#/',
             page: 0,
             slientToUrl:''
		},{
             url:'#',
             page: 0,
             slientToUrl:''
        },{
             url:'#/activity',
             page: 1,
             slientToUrl:'/#'
        },{
             url:'#/static',
             page: 2,
             slientToUrl:'/#'
        },{
             url:'#/park',
             page: 3,
             slientToUrl:'/#'
        },{
             url:'#/stores',
             page: 4,
             slientToUrl:'/#'
        },{
             url:'#/floor',
             page: 5,
             slientToUrl:'/#'
        }],
        popupOption:{
             stores:{
             	detail:{buttons:['home','back','navigate'],type_0:true},
             	navigate:{buttons:['home','back'],type_1:true}
             },
             activity:{
             	detail:{buttons:['home','back'],type_1:true}
             },
             floor:{
             	storeDetail:{buttons:['home','back'],type_1:true}
             }
        },
        slientTimespan: 3*60*1000,
        adOption:{
        	  ads:[
        	  {
	        	 adSpaces:[{
	                  name:'first',
	                  src:'/publish/resource/images/ad/1.jpg',
	                  type:'img'
	        	  }]
        	  },
        	  {
        	  	adSpaces:[{
	                  name:'first',
	                  src:'/publish/resource/images/ad/2.jpg',
	                  type:'img'
	        	  }]
        	  },
        	  {
        	  	adSpaces:[{
	                  name:'first',
	                  src:'/publish/resource/images/ad/3.jpg',
	                  type:'img'
	        	  }]
        	  },
        	  {
        	  	adSpaces:[{
	                  name:'first',
	                  src:'/publish/resource/images/ad/4.jpg',
	                  type:'img'
	        	  }]
        	  },
        	  {
        	  	adSpaces:[{
	                  name:'first',
	                  src:'/publish/resource/images/ad/5.jpg',
	                  type:'img'
	        	  }]
        	  },
        	  {
        	  	adSpaces:[{
	                  name:'first',
	                  src:'/publish/resource/images/ad/6.jpg',
	                  type:'img'
	        	  }]
        	  },
        	  {
        	  	adSpaces:[{
	                  name:'first',
	                  src:'/publish/resource/images/ad/7.jpg',
	                  type:'img'
	        	  }]
        	  },
        	  {
        	  	adSpaces:[{
	                  name:'first',
	                  src:'/publish/resource/images/ad/8.jpg',
	                  type:'img'
	        	  }]
        	  },
        	  {
        	  	adSpaces:[{
	                  name:'first',
	                  src:'/publish/resource/images/ad/9.jpg',
	                  type:'img'
	        	  }]
        	  },
        	  {
        	  	adSpaces:[{
	                  name:'first',
	                  src:'/publish/resource/images/ad/10.jpg',
	                  type:'img'
	        	  }]
        	  }
        	],
        	  option:{
                 dur: 200000
        	  }
        },
		park: [{
			ground: {
				x: 304,
				y: 59
			},
			underground: {
				x: 308,
				y: 32
			}
		}, {
			ground: {
				x: 333,
				y: 65
			},
			underground: {
				x: 399,
				y: 35
			}
		}, {
			ground: {
				x: 434,
				y: 82
			},
			underground: {
				x: 431,
				y: 70
			}
		}, {
			ground: {
				x: 278,
				y: 145
			},
			underground: {
				x: 295,
				y: 126
			}
		}, {
			ground: {
				x: 386,
				y: 147
			},
			underground: {
				x: 391,
				y: 140
			}
		}, {
			ground: {
				x: 506,
				y: 148
			},
			underground: {
				x: 526,
				y: 111
			}
		}, {
			ground: {
				x: 244,
				y: 253
			},
			underground: {
				x: 262,
				y: 268
			}
		}, {
			ground: {
				x: 353,
				y: 193
			},
			underground: {
				x: 384,
				y: 209
			}
		}, {
			ground: {
				x: 496,
				y: 266
			},
			underground: {
				x: 502,
				y: 261
			}
		}],
        startXY: {
			x: 479,
			y: 107
		},
		_3dParkEntry: {
			x: 244,
			y: 252
		}
	};

	window._Const = _Const;
	window._Const._host = _host;

})(window);
