App.BottomMapComponent = Em.Component.extend({

	didInsertElement: function() {
		var self = this;
		var paper = Raphael('mapDiv');
		MapLoader.loadMap({
			url: _Const.url.getBottomMapFromLocal, 
			showFacilities: true,//是否需要显示公共设施
			loadFromLocal: true,//是否从本地读取地图
			showSideButtons: true,//是否显示旁边的公共设施按钮
			paper: paper,
			callback: function() {
				self.sendAction('setPaper', paper);
			}
		});
	},

	willDestroyElement: function() {
		
	}
});
