App.AdRotationComponent = Em.Component.extend({

	didInsertElement: function() {
        
		var self = this;

		if(!App.adRotation)
	    {
	        App.adRotation = new Utils.adRotation(_Const.adOption.ads,$('.ad'),_Const.adOption.option);
	    }

	    App.adRotation.container=$('.ad');
        
        $(".r-page").addClass('openpage');

        App.adRotation.rotationToNext(_Const.adOption.option.dur,'index');
	},

	actions: {
		backToIndex: function() {
			var self = this;
			 $(".r-page").removeClass("openpage").addClass("closepage");
	          Utils.PrefixedEvent($(".r-page"), "AnimationEnd", function() {
	              App.Router.router.transitionTo('index');
	             
	          });
		}
	},

	willDestroyElement: function() {
		GC.clearIntervals();
	}
});