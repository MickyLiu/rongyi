App.ParkNavComponent = Em.Component.extend({
	didInsertElement: function() {
		var self = this;
		var _finder = new PF.AStarFinder();
		self.$('[class*="Blur-box"], [class*="d-but"]').addClass('Blur-box-fade-in');
		var isUndergroundLot = this.get('controller').get('isUndergroundLot');

		//加载上面的大宁广场地图
		var topPaper = Raphael('squareMap');
		MapLoader.loadMap({
			url: _Const.url.getBottomMapPic, 
			loadFromLocal: false,//是否从本地读取地图
			isBigImage: false,
			paper: topPaper,
			callback: function() {
				
			}
		});


		var bottomPaper = Raphael('undergroundMap');
		if (isUndergroundLot) {//画地下车库
			MapLoader.loadMap({
				url: _Const.url.getUndergroundLotMap, 
				loadFromLocal: false,//是否从本地读取地图
				isBigImage: false,
				paper: bottomPaper,
				callback: function() {

                    var data=self.data,
                        dx=data.logo_x,
                        dy=data.logo_y,
                        _finder = new PF.AStarFinder();
                    var path= _finder.findPath(_Const.startXY.x, _Const.startXY.y, _Const.park[0].ground.x, _Const.park[0].ground.y, _Const.path.F1.clone());
                    var enter=_Const.park[0];

                    //寻找最近入口
                    for(var i=0;i< _Const.park.length;i++){
                        var tempE=_Const.park[i];
                        var tempP=_finder.findPath(_Const.startXY.x, _Const.startXY.y, tempE.ground.x, tempE.ground.y, _Const.path.F1.clone());
                        if(tempP.length<path.length){
                            enter=tempE,path=tempP;
                        }
                    }
                    
                    Path.drawPath(topPaper,path,function(){
                        var bottomPath=_finder.findPath(enter.underground.x, enter.underground.y,dx,dy, _Const.path.Park.clone());
                        Path.drawPath(bottomPaper,bottomPath);
                    });
				}
			});
		} else {//画立体车库
			MapLoader.loadMap({
				url: _Const.url.get3DParkInfoMap, 
				loadFromLocal: true,//是否从本地读取地图
				isBigImage: false,
				paper: bottomPaper,
				callback: function() {
                    var dx=244,
                        dy=257,
                        _finder = new PF.AStarFinder();
                    var path= _finder.findPath(_Const.startXY.x, _Const.startXY.y, dx, dy, _Const.path.F1.clone());
                    
                    Path.drawPath(topPaper,path);
	
				}
			});
		}

	},


	actions: {
		hideDetail: function() {
			var self = this;
			this.$('.Blur-box, .Blur-box-main, .d-but').removeClass('Blur-box-fade-in').addClass('Blur-box-fade-out');
			setTimeout(function() {
				self.sendAction('hideDetail');
			}, 800);
		},
		gotoIndex: function() {
			this.sendAction('gotoIndex');
		}
	}
});
