App.PopupMapComponent = Em.Component.extend({

	didInsertElement: function() {
		var self = this;
		var store = self.get('store');
		self.set('buttonOption', _Const.popupOption.stores.navigate);
		self.$('[class*="Blur-box"], [class*="d-but"]').addClass('Blur-box-fade-in');
		var _finder = new PF.AStarFinder();
		var paper = Raphael('d_map');
		MapLoader.loadMap({
			url: _Const.url.getBottomMapPic, 
			loadFromLocal: false,//是否从本地读取地图
			isBigImage: false,
			paper: paper,
			callback: function() {
				var icon = store.icon;
				var x = store.logo_x;
				var y = store.logo_y;
				console.log(store);
                console.log('loaded map ... ...');
                var	tip = new com.easyGuide.Map.Tip({
		            paper: paper,
		            path: icon,
		            width: 82,
		            height: 82,
		            tipType: LOCAL_DATA.CONTENTTYPE.TIP_TYPE_SIMPLE,
                    scale:0.66
		        }, {
		        	updateTimerStatus: function () {}
		        });
	            tip.show(x, y, icon, store.id);
                console.log('showTip... ...');
                path = _finder.findPath(_Const.startXY.x, _Const.startXY.y, x, y, _Const.path.F1.clone());
			    Path.drawPath(paper, path);
 
			}
		});	
		
	},
	actions: {
		hidePopMap: function() {
			console.log('in pop map component');
			var self = this;
			this.$('.Blur-box, .Blur-box-main, .d-but').removeClass('Blur-box-fade-in').addClass('Blur-box-fade-out');
			setTimeout(function() {
				self.sendAction('hidePopMap');
			}, 800);
		},
		gotoIndex: function() {
			this.sendAction('gotoIndex');
		}
	},

	willDestroyElement: function() {
		
	}
});
