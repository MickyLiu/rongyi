App.StaticDetailComponent = Em.Component.extend({
	didInsertElement: function() {
		var self = this;
		var article = self.get('controller').get('article');
		self.set('buttonOption', _Const.popupOption.activity.detail);
		self.$('[class*="Blur-box"], [class*="d-but"]').addClass('Blur-box-fade-in');
	},


	actions: {
		hideDetail: function() {
			var self = this;
			this.$('.Blur-box, .Blur-box-main, .d-but').removeClass('Blur-box-fade-in').addClass('Blur-box-fade-out');
			setTimeout(function() {
				self.sendAction('hideDetail');
			}, 800);
		},
		gotoIndex: function() {
			this.sendAction('gotoIndex');
		}
	}
});