App.NavBlocksComponent = Em.Component.extend({
	actions: {
		clickServicesCenter: function() {
			console.log('clickServicesCenter');
			console.log(window.Facilities);
			if (window.Facilities && window.Facilities.ServicesCenter) {
				window.Facilities.ServicesCenter.events[0].f.call(window.Facilities.ServicesCenter);
			}
		},
		clickWashroom: function() {
			console.log('clickWashroom');
			console.log(window.Facilities);
			if (window.Facilities && window.Facilities.Washroom) {
				window.Facilities.Washroom.events[0].f.call(window.Facilities.Washroom);
			}
		}
	}
});