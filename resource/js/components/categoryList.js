App.CategoryListComponent = Em.Component.extend({

	lists: [],

	didInsertElement: function() {
		var self = this;
		$.getJSON(_Const.url.getCategory + '?callback=?').then(function(data) {
		// $.ajax({
		// 	url: '/resource/js/data/category.json'
		// }).then(function(data) {
			for (var i = 0, length = data.length; i < length; i++) {
				data[i]._index = 'a' + (i + 1);
			}
			self.set('lists', data);

			setTimeout(function() {
				//标题栏目
				var column = $(".con-left-T:lt(3)");
				var columnList = $(".con-left-list"); //每个列表
				var columnListli = $(".con-left-list ul li a");
				var columnListliImg = $(".con-left-list ul li a img");
				var columnBox = $(".nav-box");     //每个li对应的弹出框
				var columnBoxLi = $(".nav-box ul li a");    

				columnListliImg.each(function(i){
                        $(this).data('images',{icon:data[i].f_cate.icon,focus_icon:data[i].f_cate.focus_icon});
                        var content= location.href.substring(location.href.lastIndexOf('/')+1,location.href.length);
                        if($(this).parent().attr('data-content')== content)
                        {
                        	$(this).parent().addClass('checked');
                        	 $(this).attr('src',data[i].f_cate.focus_icon);
                        }
                        else
                        {
                        	$(this).attr('src',data[i].f_cate.icon);
                        }                 
				});

				self.$('.nav-box').each(function() {
					var $this = $(this);
					if ($this.find('li').length) {
						
						$this.find('li').find('a').addClass("bottomNode subCategory");
					} else {
						//$this.prev('a').addClass("bottomNode category");
					}
					$this.prev('a').addClass("bottomNode category");
				});

				//调用可查到的店铺首字母接口
				$.getJSON(_Const.url.getAvailableLetters).then(function(data) {
					//["1","7","a","b","c","d","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","v","w","x","y","z"]
					var $number = $('li.max:last');
					if (/\d/.test(data.join())) {
						$number.find('a').addClass('bottomNode').addClass('alph');
					} else {
						$number.addClass('un_click').find('a').remove();
					}
					for (var i = 0, len = data.length; i < len; i++) {
						var l = data[i];
						if (/\d/.test(l)) {//数字的话跳掉不判断
							continue;
						}
						$('[data-value="' + l + '"]').addClass('bottomNode').addClass('alph');
					}
					$('.letter li[class!="max"]').each(function() {
						var $this = $(this);
						var t = $this.find('a').text().toUpperCase();
						if (!$this.find('a.bottomNode').length) {
							$this.find('a').remove();
							$this.addClass('un_click').text(t);
						}
					});

					//点击左侧的列表进行分类查询
					self.$('.bottomNode').on('click', function() {
						console.log('in click bottomNode');
						var $this = $(this);
						var queryObj = {};
						//查询的类别和值
						if ($this.hasClass('address')) {//查看门牌号
							console.log('in address')
							self.sendAction('showAddress');
							return false;
						} else if ($this.hasClass('subCategory')) {//按分类查
							queryObj.type = _Const.text.category;
							queryObj.content = $this.attr('data-content');
							self.sendAction('categoryQuery', queryObj);
						} else if ($this.hasClass('alph')) {//按字母查
							queryObj.type = _Const.text.letter;
							queryObj.content = $this.attr('data-value').toLowerCase();
							self.sendAction('categoryQuery', queryObj);
						} else {//按分类查
                              
                              if($this.next().find('li').length==0)
                              {
                                  queryObj.type = _Const.text.category;
							      queryObj.content = $this.attr('data-content');
							      self.sendAction('categoryQuery', queryObj);
                              }

							/*queryObj.type = _Const.text.isFavored;
							queryObj.content = true;*/
						}
						
					});	
				});


				//标题栏目点击触发
                var isAnimate=false;
                column.each(function(i){
                	$(this).bind('click',function(){
                		if(!isAnimate){
                			isAnimate=true;
                			if(!$(this).hasClass('checked'))
                			{
                                $(column).removeClass("checked");
						        $(this).addClass("checked");
						        var current=$(".con-left-list:nth("+ i +")");
						        if(current.length==0)
						        {
                                   $(".con-left-list").slideUp(500,function(){
                                   	isAnimate=false;
                                   });
                                   $(this).css("margin-bottom","10px");
						        }
						        else
						        {
                                   current.slideDown(500);
						           current.siblings('.con-left-list').slideUp(500,function(){
						           	  isAnimate=false;
						           });
						        }
                			}
                			else
                			{
                				var current=$(".con-left-list:nth("+ i +")");
                                var c=$(this);
                				current.slideToggle(500,function(){
                                  c.removeClass("checked");
                                  isAnimate=false;
                				});
                			}
                		}
                	});
                });

				//标题栏目子列表ul li a点击时触发

				columnListli.on('click', function(){

					var Box_length = $(this).next(columnBox).find("li").length;
					if( Box_length == 0){
						$(this).next(columnBox).remove();
					}

                     var bSubTitle = $(this).hasClass('subCategory');
                     var bCategoryClick = $(this).hasClass('category') || $(this).hasClass('subCategory');

                     if(!bSubTitle && bCategoryClick)
                     {
	                     var cimg=$(".con-left-list ul.categoryList li a.checked img");
	                     if(cimg.length>0)
	                     {
	                     	cimg.attr('src',cimg.data('images').icon);
	                     }
                     }

                     if(!bCategoryClick)
                     {
                     	 var cimg=$(".con-left-list ul.categoryList li a.checked img");
	                     if(cimg.length>0)
	                     {
	                     	cimg.attr('src',cimg.data('images').icon);
	                     }
                     }

                    $(columnListli).removeClass("checked");
					$(this).addClass("checked");

                     if(!bSubTitle &&　bCategoryClick)
                     {
	                     var icon=$(this).find('img');
	                     if(icon.length>0)
	                     {
	                     	icon.attr('src',icon.data('images').focus_icon);
	                     }
                     }
                         
					$(columnBox).hide("slow");
					$(this).next(columnBox).show("slow");
					$(this).next(columnBox).find("li:even").addClass("border-left");
					$(this).next(columnBox).find("li").eq(0).addClass("border-first");

					//弹出框内的触发
					columnBoxLi.one('click', function(){
						var $this = $(this);
						columnBoxLi.removeClass("checked").removeClass("Achecked");
						$this.addClass("Achecked");
						var temp = $this.parent().parent().parent();				
						temp.show().hide("slow");
						temp.prev().addClass("checked").show("slow");
						temp.prev().parent().addClass("border-right").show("show");
						temp.parent().removeClass("border-right");
					});
				});


				
				
			}, 0);
			
		});
	}
});