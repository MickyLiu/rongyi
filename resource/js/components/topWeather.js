App.TopWeatherComponent = Em.Component.extend({

	didInsertElement: function() {
        
		var self = this;

		$.getJSON(_Const.url.getWeather + '?callback=?').then(function(data) {
			var temp = data.weatherinfo.st1;
			var tempSpan = data.weatherinfo.temp1;
			var currentWeather = data.weatherinfo.weather1;
			var weekdays = new Array(7);
			weekdays[0]="周日";
			weekdays[1]="周一";
			weekdays[2]="周二";
			weekdays[3]="周三";
			weekdays[4]="周四";
			weekdays[5]="周五";
			weekdays[6]="周六";


			var daysToShow=[];
			var days = 0;
			var dayTime= 24*60*60*1000;
			var dayCount = 0;
			var currentDate= new Date();
			while(days<4)
			{
			   var date = new Date(currentDate.getTime()+(dayCount*dayTime));
               var day = date.getDay();
			   daysToShow.push({day:weekdays[day],weather:data.weatherinfo['weather'+(days+1)]});
               days++;
               dayCount++;
			}

			console.log(daysToShow);

			//天气判断图片

            var matchWeather=function(weather,dom){
	             if(weather.match("晴"))
				{
					if(weather.match("多云") || weather.match("阴"))
					{
	                    dom.attr("src","resource/images/weather/02.png");
					}
					else if(weather.match("雨"))
					{
						if(weather.match("雷"))
						{
	                       dom.attr("src","resource/images/weather/13.png");
						}
						else
						{
							dom.attr("src","resource/images/weather/03.png");
						}
					}
					else if(weather.match("雪"))
					{
	                     dom.attr("src","resource/images/weather/10.png");
					}
					else
					{
	                    dom.attr("src","resource/images/weather/01.png");
					}
				}
				else if(weather.match("阴") || weather.match("多云"))
				{
					if(weather.match('雨'))
					{
	                    dom.attr("src","resource/images/weather/05.png");
					}
					else if(weather.match("雪"))
					{
						dom.attr("src","resource/images/weather/15.png");
					}
					else
					{
						if(weather.match("阴"))
						{
	                        dom.attr("src","resource/images/weather/06.png");
						}
						else
						{
	                        dom.attr("src","resource/images/weather/04.png");
						}
					}      
				}
				else if(weather.match("雨"))
				{
					if(weather.match("雷"))
					{
						dom.attr("src","resource/images/weather/12.png");
					}
					else if(weather.match('暴') || weather.match("大") || weather.match("阵"))
					{
	                     dom.attr("src","resource/images/weather/09.png");
					}
					else if(weather.match("中"))
					{
	                   dom.attr("src","resource/images/weather/08.png");
					}
					else if(weather.match("小"))
					{
	                   dom.attr("src","resource/images/weather/07.png");
					}
					else if(weather.match("雪"))
					{
						 dom.attr("src","resource/images/weather/11.png");
					}
					else
					{
	                   dom.attr("src","resource/images/weather/07.png");
					}
				}
				else if(weather.match("冰雹"))
				{
	                dom.attr("src","resource/images/weather/14.png");
				}
				else if(weather.match("雪"))
				{
					if(weather.match("小"))
					{
						dom.attr("src","resource/images/weather/10.png");
					}
	                 else if(weather.match("中"))
					{
	                   dom.attr("src","resource/images/weather/18.png");
					}
					else if(weather.match("大") || weather.match('暴'))
					{
	                   dom.attr("src","resource/images/weather/19.png");
					}
					else
					{
						dom.attr("src","resource/images/weather/10.png");
					}
				}
				else if(weather.match("雾") || weather.match("霾"))
				{
	                 dom.attr("src","resource/images/weather/17.png");
				}
				else
				{
					dom.attr("src","resource/images/weather/01.png");
				}
            }


            $('.location').text(data.weatherinfo.city);
            $('.currentDate').text(new Date().toISOString().split('T')[0]);

            $('.temperature em').text(temp);
            $('.temperature sup').text('°');
            $('.data p').text(tempSpan.split('~')[1]+'~'+tempSpan.split('~')[0]);
            $('.data span.today').text(daysToShow[0].day);
            matchWeather(daysToShow[0].weather,$(".weather >img"));

            $(".weather >span").each(function(i){
                   matchWeather(daysToShow[i+1].weather,$(this).find('img'));
                   $(this).find('span').text(daysToShow[i+1].day);
            });

			//天气判断图片end

		});


		//时钟初始化


		var clock1   = Snap("#myclock");
		
		var hours1   = clock1.rect(79, 35, 4, 55).attr({fill: "#2ab6db", transform: "r" + 10 * 30 + "," + 80 + "," + 80});
		var minutes1 = clock1.rect(79, 20, 2, 70).attr({fill: "#2ab6db", transform: "r" + 10 * 6 + "," + 80 + "," + 80});
		var seconds1 = clock1.rect(80, 10, 1, 80).attr({fill: "#2ab6db"});
		var middle1 =   clock1.circle(80, 80, 5).attr({fill: "#2ab6db"});
		var middle2 =   clock1.circle(80, 80, 2).attr({fill: "#fff"});
        
        
		// CLOCK Timer
		var updateTime = function(_clock, _hours, _minutes, _seconds) {
			var currentTime, hour, minute, second;
			currentTime = new Date();
			second = currentTime.getSeconds();
			minute = currentTime.getMinutes();
			hour = currentTime.getHours();
			hour = (hour > 12)? hour - 12 : hour;
			hour = (hour == '00')? 12 : hour;

            if($('#myclock').length>0)
            {
				if(second == 0){
					//got to 360deg at 60s
					second = 60;
				}else if(second == 1 && _seconds){
					//reset rotation transform(going from 360 to 6 deg)
					_seconds.attr({transform: "r" + 0 + "," + 80 + "," + 80});
				}
				if(minute == 0){
					minute = 60;
				}else if(minute == 1){
					_minutes.attr({transform: "r" + 0 + "," + 80 + "," + 80});
				}
				//_hours.animate({transform: "r" + hour * 30 + "," + 80 + "," + 80}, 200, mina.elastic);
				//_minutes.animate({transform: "r" + minute * 6 + "," + 80 + "," + 80}, 200, mina.elastic);
				_hours.attr({transform: "r" + hour * 30 + "," + 80 + "," + 80});
				_minutes.attr({transform: "r" + minute * 6 + "," + 80 + "," + 80});
				if(_seconds){
					_seconds.animate({transform: "r" + second * 6 + "," + 80 + "," + 80}, 500, mina.elastic);
					//_seconds.attr({transform: "r" + second * 6 + "," + 80 + "," + 80});
				}
			}
		}
		var updateSeconds = function(_clock, _seconds){
			var currentTime, second;
			currentTime = new Date();
			second = currentTime.getSeconds();

			if(second == 0){
				//got to 360deg at 60s
				second = 60;
			}else if(second == 1 && _seconds){
				//reset rotation transform(going from 360 to 6 deg)
				_seconds.attr({transform: "r" + 0 + "," + 80 + "," + 80});
			}
			if(_seconds){
				_seconds.attr({transform: "r" + second * 6 + "," + 80 + "," + 80});
			}
		}

		updateTime(clock1, hours1, minutes1, seconds1);

		//update the clocks
		var clockInterval= setInterval(function(){
			updateTime(clock1, hours1, minutes1, seconds1);
		}, 1000);
      
        GC.addInterval(clockInterval);
     
	},

	willDestroyElement: function() {
		GC.clearIntervals();
	}
});