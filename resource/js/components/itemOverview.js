App.ItemOverviewComponent = Em.Component.extend({
	actions: {
		showMap: function(shop) {
			console.log(shop);
			var oFacility = {};
			var self = this;
			var x = shop.logo_x;
			var y = shop.logo_y;
			var _finder = new PF.AStarFinder();

            var params = [0, 0, 1028, 680];
        	var insertImage = function(imgSrc, paper) {
				var paramList = params.concat();
				paramList.unshift(imgSrc);
				return paper.image.apply(paper, paramList);
			};

			$.getJSON(_Const.url.getBottomMapPic + '?callback=?').then(function(data) {
				self.sendAction('showMap');
				setTimeout(function() {
						console.log('in timeout');
						var innerPaper = Raphael('mapNav');
						var facilitiesSet = innerPaper.set();
						var rBg = insertImage(data.bg, innerPaper);
						facilitiesSet.push(rBg);
                        var px=1024*0.66/4,py=680*0.66/4,ppx=15,ppy=5;
                            rBg.attr({transform:['t',-px,-py,'s',0.66]});
                        var _width=_Const.imageUrl.location.width,
                            _height=_Const.imageUrl.location.height,
                            lx=_Const.startXY.x-(_width/2),
                            ly=_Const.startXY.y-(_height/2)-_Const.imageUrl.location.arrow;
                            console.log(lx*0.66);
                        var mark=innerPaper.image(_Const.imageUrl.location.path,lx*0.66-ppx,ly*0.66-ppy,_width,_height);
                            mark.attr({ transform: ["t",0 , 0,"s",0.66] });
                            setInterval(function () {//临时放置记得清除
                            mark.animate({ transform: ["t", 0, - 10,'s',0.66] }, 490, function () {
                                mark.animate({ transform: ["t", 0, 0,'s',0.66] }, 340);
                            })
                        }, 840);
                
					
						if (self.get('currentIcon')) {
							self.get('currentIcon').remove();
						}

						var shopIcon = shop.icon;
						var x = shop.logo_x;
						var y = shop.logo_y;

                        var ntip = new com.easyGuide.Map.Tip({
		                        paper: innerPaper,
		                        path: shopIcon,
		                        width: 82,
		                        height: 82,
		                        tipType: LOCAL_DATA.CONTENTTYPE.TIP_TYPE_SIMPLE,
                                scale:0.66
		                    }, {
		        	            updateTimerStatus: function () {}
		                    });
	                    ntip.show(x*0.66+6, y*0.66+4, shopIcon, '123123');

						path = _finder.findPath(_Const.startXY.x, _Const.startXY.y, x, y, _Const.path.F1.clone());

						Path.drawPath(innerPaper, path, 'mapNav',0.66);

					}, 0);
			});
		},
		showPopMap:function(obj){
           this.sendAction('showPopMap', obj);
		},
		showDetailCom: function(obj) {
			this.sendAction('showDetailCom', obj);
		}
	}
});
