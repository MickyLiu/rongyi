App.StoreDetailComponent = Em.Component.extend({

	didInsertElement: function() {
		var self = this;
		var store = self.get('store');
		self.set('buttonOption', window.navStore.option);
		self.$('[class*="Blur-box"], [class*="d-but"]').addClass('Blur-box-fade-in');
		/*var _finder = new PF.AStarFinder();
		var paper = Raphael('d_map');
		MapLoader.loadMap({
			url: _Const.url.getBottomMapPic, 
			loadFromLocal: false,//是否从本地读取地图
			isBigImage: false,
			paper: paper,
			callback: function() {
				var icon = store.icon;
				var x = store.logo_x;
				var y = store.logo_y;
				console.log(store);
                console.log('loaded map ... ...');
                var	tip = new com.easyGuide.Map.Tip({
		            paper: paper,
		            path: icon,
		            width: 82,
		            height: 82,
		            tipType: LOCAL_DATA.CONTENTTYPE.TIP_TYPE_SIMPLE,
                    scale:0.66
		        }, {
		        	updateTimerStatus: function () {}
		        });
	            tip.show(x, y, icon, store.id);
                console.log('showTip... ...');
                path = _finder.findPath(_Const.startXY.x, _Const.startXY.y, x, y, _Const.path.F1.clone());
			    Path.drawPath(paper, path);*/

 
			    /*MapLoader.insertImage({
					paper: paper,
					imgSrc: icon,
					params: [x, y].concat(MapLoader.p.iconParams),
					callback: function() {
						try {
							//console.log(_Const.startXY_small.x + ':' + _Const.startXY_small.y);
							//path = _finder.findPath(_Const.startXY_small.x, _Const.startXY_small.y, x, y, _Const.path.F1.clone());
							//Path.drawPath(paper, path);
						} catch (e) {
							console.log(store.name + '找不到可以行进的路径');
						}
						
					}
				});
			}
		});	
		
		//self.$('.d-pic li').hide();
		//self.$('.d-pic li:first').show();

		//详情页轮播动画开始
		/*var picImg = $(".d-pic-img");
		var picImgArea = $(".d-pic-img-area");
		var picNum = $(".d-pic-img-area").find("li").length;
		var i = 0;
		var leftSize = 640;

		if (picNum > 1) {
			var ii = setInterval(picImg_effect,2500);
			GC.addInterval(ii);
			self.set('interval', ii);
		}
		function picImg_effect(){
			i++;
			if (i == picNum) {
				i = 0;
			}
			$(".d-pic-img-area").animate({"width":picNum*leftSize+"px"});
			// $(".d-pic-img-area").animate({"left": i * -leftSize + "px"},"fast");
			$(".d-pic-img-area").animate({left: i * -leftSize,opacity:"0.9"},300).animate({left: i * -leftSize,opacity:"1"},300);
		}*/


         var slides = $('.d-pic-img-area li');
         window.myslide = Utils.imageSlidey($('.d-pic-img-area'),slides,{dur:3000});
        
		//详情页轮播动画结束
         
        //详情页导航按钮 
	     setTimeout(function() {
			$('.menu').click(function(){   
	             $(".r-page").removeClass("openpage").addClass("closepage");
		        Utils.PrefixedEvent($(".r-page"), "AnimationEnd", function() {
		            App.Router.router.transitionTo('floor');
		          });
			});
		},10);

		
	},
	actions: {
		hideDetail: function() {
			console.log('in store detail component');
			var self = this;
			this.$('.Blur-box, .Blur-box-main, .d-but').removeClass('Blur-box-fade-in').addClass('Blur-box-fade-out');
			setTimeout(function() {
				self.sendAction('hideDetail');
			}, 800);

		},
		gotoIndex: function() {
			this.sendAction('gotoIndex');
		}
	},
	willDestroyElement: function() {
		console.log('will destroy element');
		window.myslide.stop();
		window.myslide= null;
		//$('.d-pic-img-area').data('unslider').stop();
		//GC.clearIntervals(this.get('interval'));
	}
});
