App.NavBarComponent = Em.Component.extend({
	didInsertElement: function() {
		var self = this;
		var title;
		//判断是否有title属性，没有，则代表是静态页面，从url中的/static/后取得title
		if (self.get('title')) {
			title = self.get('title');
		} else {
			title = location.href.split('/static/')[1];
		}
		$.getJSON(_Const.url.getNavBarInfo + '?callback=?&title=' + title).then(function(data) {
			self.set('className', data.title);
			self.set('title', data.name);
		});
	},
	actions: {
		backToIndex: function() {
			var self = this;
			$(".r-page").removeClass("openpage").addClass("closepage");
			Utils.PrefixedEvent($(".r-page"), "AnimationEnd", function() {
				App.Router.router.transitionTo('index');
			});
		}
	}
});