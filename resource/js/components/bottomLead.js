App.BottomLeadComponent = Em.Component.extend({

	leads: [],
	didInsertElement: function(){
		var self = this;
		$.getJSON(_Const.url.getIndexCategory + '?callback=?').then(function(data){
			
			self.set('leads', data);
			setTimeout(function() {
				var leadListImgBig = $(".nav_lead >img");
				var leadListImgSmall = $(".nav_lead >div img");

				leadListImgBig.each(function(i){
					$(this).attr('src',data[i].desc_img);
				});
				leadListImgSmall.each(function(i){
					$(this).attr('src',data[i].icon);
				});

                    $('bottom_lead').removeClass('bounceIn').addClass('bounceIn');
                    $('#bottom_lead div.nav_lead').each(function(i){
						$(this).addClass('bottom_'+(i+1));
						$(this).find('div').addClass('bottom_'+ (i+1) +'_1');	
						$(this).click(function(){
                                Utils.jumpFromIndex();
                                var ele=$(this);
								Utils.PrefixedEvent($(".wrap-main"), "AnimationEnd", function() {
									  App.Router.router.transitionTo('stores', ele.attr('data-content'));
								});
						});
	         	   });
			},0);
		});
	}
});
 