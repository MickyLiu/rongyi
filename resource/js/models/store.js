/*
 *
 * 定义店铺模型类
 *
 */
App.Store = Ember.Object.extend({
	icon: null,	//图标
	img: null,	//大图
	actualImg: null,	//实景图
	name: 'emty',	//店铺名称
	category: 'empty',	//大分类，零售、娱乐、餐饮等大类
	type: 'empty',		//小分类，日式、西式、饮料、甜点等小类
	floor: ['F1'],		//所属楼层
	locationCode: '001',	//门牌号
	tag: [],			//所属标签
	desc: 'empty',		//详细描述
	mapInfo: null		//地图属性对象
});

App.Store.reopenClass({
	find: function(id) {

	}
});