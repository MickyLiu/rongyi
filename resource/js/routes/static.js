App.StaticRoute = Em.Route.extend({

	model: function(param) {
		 if(window.pageNavigator)
        {
        	window.pageNavigator.currentPage = 2;
        }
		console.log('----------------------------------entered static route-----------------------------------------');
		console.log('currentPage:2');
		// 进入其他页重新计时
        clearTimeout(window.pageNavigator.timeoutId);
        window.pageNavigator.timeoutId= Utils.countToUserLeave.startCount();
        
		return $.getJSON(_Const.url.getStaticPic + '?callback=?&title=' + param.title).then(function(data) {
			return data;
		});
	}

	// setupController: function(controller, model) {
	// 	controller.set('model', model);
	// }
});