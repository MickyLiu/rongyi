App.ActivityRoute = Em.Route.extend({
	model: function(param) {
        if(window.pageNavigator)
        {
        	window.pageNavigator.currentPage = 1;
        }
		console.log('----------------------------------entered static route-----------------------------------------');
		console.log('currentPage:1');
		this.set('defaultTitle', param.title);

		// 进入其他页重新计时
        clearTimeout(window.pageNavigator.timeoutId);
        window.pageNavigator.timeoutId= Utils.countToUserLeave.startCount();
        
		//测试用本地data下的activity.json
		return $.getJSON(_Const.url.getActivityDetail).then(function(data) {
			return data;
		});
	},
	setupController: function(controller, model) {
		console.log('in activity route');
		console.log(model);
		controller.set('model', model);
		controller.set('defaultTitle', this.get('defaultTitle'));
	}
});
