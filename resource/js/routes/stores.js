App.StoresRoute = Ember.Route.extend({
	content_id:"all",
	model: function(params) {
		 if(window.pageNavigator)
        {
        	window.pageNavigator.currentPage = 4;
        }
		console.log('----------------------------------entered stores route-----------------------------------------');
		console.log('currentPage:4');
		//console.log(_Const.url.getStores + '?callback=?&page=1&type=category&content=1');


		// 进入其他页重新计时
        clearTimeout(window.pageNavigator.timeoutId);
        window.pageNavigator.timeoutId= Utils.countToUserLeave.startCount();
        //设置默认筛选类别

        this.set('content_id',params.content_id);
        //content_id=params.content_id;

		return $.getJSON(_Const.url.getStores + '?callback=?&page=1&type=category&content='+params.content_id).then(function(data) {
			console.log(data)
			return data;
		});
	},
	setupController: function(controller, model) {
		controller.set('mapId', 'd_map');
		controller.set('model', model);
		controller.set('content_id',this.get('content_id'));
		controller.set('article', {
			src: _Const.imageUrl.address
		});
	}
	// setupController: function(controller, model) {
	// 	console.log(_Const.url.getStores + '?callback=?&page=1&type=category&content=1');
	// 	$.getJSON(_Const.url.getStores + '?callback=?&page=1&type=category&content=1').then(function(data) {
	// 		controller.set('model', data);
	// 	});
	// }
});