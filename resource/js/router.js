/*
 *
 * 路由定义文件
 *
 */
// App.Router.reopen({
//   location: 'auto'
// })
 
App.Router.map(function() {
	//this.route('stores');		//品牌导购
	this.route('stores',{path:'stores/:content_id'});
	this.route('static', {path: '/static/:title'});		//静态展示页面
	this.route('park');		//停车位
	this.route('floor');	//楼层导购
	this.route('activity', {path: '/activity/:title'});	//精彩活动
	this.route('ad');
});
