App.FloorView = Em.View.extend({
	didInsertElement: function() {
		var self = this;
		$(".r-page").removeClass("closepage").addClass("openpage");

		/*//楼层导航右侧切换
		function variables(){
			 up = $("#up");
			 down = $("#down");
			 list = $(".f-logoR-logo-m-list");
			 SumLi = $(".f-logoR-logo-m-list ul li");
			 list.css('top', '0px');
			
			 pre = $("#pre");
			 next = $("#next");
			 leftlist= $(".f-logoL-m-list");
			 leftSumLi= $("#floors li");			
			 i = 0;
			 num = Math.ceil(SumLi.length/SingleLi)-1;
		}

		var up = $("#up");
		var down = $("#down");
		var list = $(".f-logoR-logo-m-list");
		var SumLi = $(".f-logoR-logo-m-list ul li");
		var SingleLi = 32;
		var topHeight = -460;
		var i = 0;
		var num = Math.ceil(SumLi.length/SingleLi)-1;

		up.click(function(){
			down_page();
		});
		down.click(function(){
			up_page();
		});
		function up_page(){
			if(i == num){
				return false;
			}
			i++;
			list.animate({'top':i*topHeight+"px"});	
		}
		function down_page(){
			if(i == 0){
				return false;
			}
			i--;
			list.animate({'top':i*topHeight+"px"});	
		}
		//楼层导航右侧切换end
		//楼层导航左侧切换
		var pre = $("#pre");
		var next = $("#next");
		var leftlist= $(".f-logoL-m-list");
		var leftSumLi= $("#floors li");
		var leftSingleLi = 1;
		var lefttopHeight = -76;
		var leftnum = leftSumLi.length-5; //判断多屏or 一屏
		var currentLeftLi = leftSumLi.eq(0);
		var m = 0;

		//点击左侧的楼层，改变右侧商铺列表内容界面
		leftSumLi.click(function() {
			var $this = $(this);
			if ($this == currentLeftLi) {
				return false;
			}
			leftSumLi.removeClass('checked');
			$this.addClass('checked');
			//改变控制器当前楼层的索引
			var i = $this.prevAll('li').length;
			self.get('controller').send('setCurrentFloorIndex', i);
			setTimeout(function() {
				variables();
			}, 100);
		});
		$('#floors li:last').click();
		m=4;
		pre_page(m);

		pre.click(function(){
			next_page();
		});
		next.click(function(){
			pre_page();
		});
		function pre_page(a){
			if(a)
			{
				leftlist.animate({'top':a*lefttopHeight+"px"});
			}
			else
			{
                  if(leftnum <= 0){
				return false;
				}
				if(m == leftnum){
					return false;
				}
				m++;
				leftlist.animate({'top':m*lefttopHeight+"px"});	
			}
		}
		function next_page(){
               if(m == 0){
				return false;
			}
			m--;
			leftlist.animate({'top':m*lefttopHeight+"px"});
		}
*/
		//楼层导航右侧切换endMath.round(bb.y)
		//初始化
		//currentLeftLi.click();
      
	   GIM.goDetail = function(shopRoom){
	   	    $.getJSON(_Const.url.getShopByShopRoom+shopRoom).then(function(data) {
                     window.navStore = {store:data,option:_Const.popupOption.floor.storeDetail};
                     $('.goStoreDetial').click();
            });
	       
	    }
      
		  var map = new GIM.Map3D($("#mapContainer")[0]);
		  if(window.navStore)
		  {
		  	 GIM.navitateTo(window.navStore.store.locationCode);
		  }
		
	},
	actions: {
		showTip: function(shop) {
			var self = this;
			self.get('controller').set('showDetail', false);
			var path = shop.icon,
            	x = shop.logo_x,
				y = shop.logo_y,
            	code = shop.id;
	        var paper = self.get('controller').get('paper');
	        var oFacility = {};

	        if (this.get('tip')) {
	        	this.get('tip').hide();
	        }
	        var	tip = new com.easyGuide.Map.Tip({
		            paper: paper,
		            path: path,
		            width: 82,
		            height: 82,
		            tipType: LOCAL_DATA.CONTENTTYPE.TIP_TYPE_ALL,
                    sectorType:LOCAL_DATA.CONTENTTYPE.TIP_SECTOR_IMG
		        }, {
		        	updateTimerStatus: function () {}
		        });
	        tip.show(x/0.66, y/0.66, path, code);
        	this.set('tip', tip);

        	tip.frame.area.sectorSet[0].click(function() {//查看详情
        		console.log(0);
        		self.get('controller').send('showStoreDetail', shop);
        		// showDetail(shop);
        	});
        	tip.frame.area.sectorSet[1].click(function() {//查看详情
        		console.log(1);
        		self.get('controller').send('showStoreDetail', shop);
        		// showDetail(shop);
        	});
        	tip.frame.area.sectorSet[2].click(function() {//怎么走
        		console.log(2);
        		self.get('controller').send('showStoreDetail', shop);
        		// showMapNav(shop);
        	});
        	tip.frame.area.sectorSet[3].click(function() {//怎么走
        		console.log(3);
        		self.get('controller').send('showStoreDetail', shop);
        		// showMapNav(shop);
        	});
		}
	},

	willDestroyElement:function(){
         console.log('will destroy element');
         window.navStore = null;
		 GC.clearIntervals();
	}


});
