App.IndexView = Em.View.extend({
	

	didInsertElement: function() {
		//console.log('----------------------------------entered index route-----------------------------------------');
		var self = this;
		Utils.initAnimate();

		$('[data-route]').click(function() {
			var $this = $(this);
			var title = null;
			if ($this.attr('data-title')) {
				title = $this.attr('data-title');
			}
			var route = $this.attr('data-route');
			Utils.jumpFromIndex();

			//跳转到内容页面
			Utils.PrefixedEvent($(".top_wrap"), "AnimationEnd", function() {
				console.log('route:' + route + ':' + title);
				title == null ? self.get('controller').transitionToRoute(route) : self.get('controller').transitionToRoute('/' + route + '/' + title);
			});
		});
	},
	willDestroyElement: function() {
		console.log('will destroy element');
		GC.clearIntervals();
	}
});
