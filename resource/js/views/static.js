App.StaticView = Em.View.extend({
	didInsertElement: function() {
		console.log('-----------------entered static view------------------------');
		var _self = this;
		$(".r-page").removeClass("closepage").addClass("openpage");
		//会员中心左右切换特效

		_self.index=0,
		_self.list=$('.member-list div.member-listDiv'),
		_self.total=_self.list.size(),
		_self.preButton=$("#pre"),
		_self.nextButton=$("#next"),
		_self.marginSize=1006;

		_self.preButton.click(function(){
			if(_self.index!=0){
				_self.index--;
			}else{
				return;
			}
			_self.slide(-1);
		});
		_self.nextButton.click(function(){
			if(_self.index!=(_self.total-1)){
				_self.index++;
			}else{
				return;
			}
			_self.slide(1);
		});

		var _init=function(){
			_checkButtonStatus.call(this);
			$(this.list[0]).css('top',0);
		}

		var _checkButtonStatus=function(){
			if(this.total <= 1){
				$('#pre, #next').hide();
			}else {
				if(this.index==0){
					this.nextButton.fadeIn();
					this.preButton.fadeOut();

				}else if(this.index==(this.total-1)){
					this.nextButton.fadeOut();
					this.preButton.fadeIn();
				}else{
					$('#pre, #next').show();
				}
			}
		}

		this.slide=function(flag){
			_checkButtonStatus.call(this);
			var _currentDiv=$(_self.list[_self.index-flag]),_nextDiv=$(_self.list[_self.index]),_marginLenth=flag*_self.marginSize;			
			_nextDiv.css({'left':+_marginLenth,'top':'0'});
			_currentDiv.animate({'left':-_marginLenth},function(){
				$(this).css({'top':0});
			});
			_nextDiv.animate({'left':0});		
		}

		_init.apply(this,arguments);
	}
});
