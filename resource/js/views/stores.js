App.StoresView = Em.View.extend({
	
	didInsertElement: function() {
		console.log('-----------------------------entered stores view-----------------------------------');
		$(".r-page").removeClass("closepage").addClass("openpage");
		var self = this;
		var page = 1;


		var touchInterval;
		var scrollInterval = 2000;
		$('.con-right-m').bind('scroll', function(e) {
			var $this = $(this);
			clearInterval(touchInterval);

			touchInterval = setTimeout(function() {
				var currentScrollTop = self.get('controller').get('currentScrollTop');
				if ($this.scrollTop() >= currentScrollTop) {
					
					self.get('controller').set('currentScrollTop', currentScrollTop + scrollInterval);
					self.get('controller').send('loadMore');
				}
			}, 100);
		});
	},
	willDestroyElement: function() {
		this.$('.con-right-m').unbind('scroll');
		this.get('controller').set('currentScrollTop',400);
		this.get('controller').set('page',1);
		this.get('controller').set('currentQueryContent',1);
	},
	actions: {
		hideMapNav: function() {
			this.get('controller').set('showMapNav', false);
		}
	}
});