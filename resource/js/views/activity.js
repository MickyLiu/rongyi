App.ActivityView = Em.View.extend({

	didInsertElement: function() {
		var self = this;
		var defaultTitle = this.get('controller').get('defaultTitle');
		$(".r-page").removeClass("closepage").addClass("openpage");
		var imgTmpl = '<img src="[src]" class="activity_detail" data-title="[data-title]" data-id="[id]" activity-text="[activity-text]"/>';
		var videoTmpl = '<video src="[src]" autoplay="autoplay" loop="loop" width="720px">您的浏览器不支持 video 标签。</video>';
		var model = self.get('model');
		var $lis = $('.activities li');
		var firstClassModel = self.get('controller').get('firstClassModel');
		var secondClassModel = self.get('controller').get('secondClassModel');

		var $maxLi = $lis.filter(function() {
			return $(this).hasClass('big');
		});


		var $minLi = $lis.filter(function() {
			return $(this).hasClass('small');
		});

		var getObjTmpl = function(obj) {
			var fileType = obj.file_type;
			var thumbnail = obj.thumbnail;
			var actualImg = obj.actual_img;
			var special_file=obj.special_file;
			var id = obj.id;
			return fileType === 'mp4' ? videoTmpl.replace('[src]',special_file) : imgTmpl.replace('[src]', thumbnail).replace('[data-title]', actualImg).replace('[id]', id);
		}

		//已经在控制器中设置好了
		//firstClassModel为priority为1的对象
		//secondClassModel为priority为2的对象

		//先放置一等模型
		/*for (var i = 0, len = firstClassModel.length; i < len; i++) {
			var o = firstClassModel[i];
			var ele = getObjTmpl(o);
			var $p;
			if (o.file_type === 'mp4') {
				$p = $maxLi.last();
			} else {
				$p = $maxLi.first();
			}
			$p.append(ele);
		}*/

		$maxLi.each(function(i){
            var o = firstClassModel[i];
			var ele = getObjTmpl(o);
			if (o.file_type === 'mp4') {
			    $(this).append(ele);
		   }
		});

		//再放置二等模型
		for (var i = 0, len = secondClassModel.length; i < len; i++) {
			if (i > 4) {
				break;
			}
			var o = secondClassModel[i];
			var ele = getObjTmpl(o);
			$minLi.eq(i).find('a').append(ele);
			$minLi.eq(i).find('a img').data('detailData',o);
		}

        var liLength = $minLi.length;
		var liIndex = liLength - 1;
		var dataLength = secondClassModel.length;
		var dataIndex = 0;

		for(var from=1;from<=dataLength;from++)
	     {
	           var m = from%liLength-1;
	           var targetData = secondClassModel[from-1];
		       var targetLi = $minLi.eq(m);
		       if(typeof targetLi.data('imgDatas')!='undefined')
		       {
                    var srcs=targetLi.data('imgDatas');
                    srcs.push(targetData);
                    targetLi.data('imgDatas',srcs);
		       }
		       else
		       {
		       	  targetLi.data('imgDatas',[targetData]);
		       }
	     }
       
			
		
		//var $ele = $('.activity_detail');
		GC.addBinding($minLi);

		//弹出活动详情框
		$minLi.click(function() {
			var picFlag=$(this).data('back')?'back-flipper':'front-flipper';
			var $this = $(this).find('img.'+ picFlag);
			if($this.length==0)
			{
				$this = $(this).find('img');
			}
			var title = $.trim($this.attr('data-title'));
			if (title == '') {
				return false;
			}
            
            var detailData= $this.data('detailData');

            var timeTxt=detailData.show_time_start.split('T')[0]+'至'+detailData.show_time_end.split('T')[0];
			var article = {
				src: title,
				title: detailData.title,
				time: timeTxt,
				address:detailData.address,
				text:detailData.activity_text
			};
			self.get('controller').set('showDetail', true);
			setTimeout(function() {
				self.get('controller').set('article', article);
			}, 0);
		});


		if (defaultTitle != 'all') {
			$('[data-id="' + defaultTitle + '"]:first').click();
		}

		//console.log(secondClassModel);//对象
		//console.log($minLi); //li集合
		
        $minLi.each(function(){
        	var imgs=$(this).data('imgDatas');

        	if(imgs.length>1)
        	{
        		$(this).data('index',0);
                $(this).data('back',false);
                var ele = getObjTmpl(imgs[1]);
			    $(this).find('a').append(ele);
			    $(this).find('a img:nth(1)').data('detailData',imgs[1]);
        	}
            if($(this).find('img').length>=2)
		    {
		    	$(this).addClass('flip-container');
        	    $(this).find('a').addClass('flipper');
                $(this).find('img:nth(0)').addClass('front-flipper');  
                $(this).find('img:nth(1)').addClass('back-flipper');  
                //$(this).find('img:nth(1)').nextAll().addClass('non-flipper'); 
		    }
        });

        
        var current=$minLi.eq(0);
        var flag=0;
        var round=0;
		var changeImg = setInterval(function() {

               if(current.find('img').length>1)
               {
                  var data = current.data('imgDatas');
                  var bBack=current.data('back');

                  bBack=!bBack;
                  current.data('back',bBack);
                  if(data.length>2)
                  {
                  	   var index=current.data('index');
	               	   index++;
	               	   if(index>=data.length)
	               	   {
	               	   	  index=0;
	               	   }
	               	   current.data('index',index);
	               	   
	                   if(bBack)
	                   {
	                      current.find('.back-flipper').attr({
                                 'data-id':data[index].id,
                                 'data-title':data[index].actual_img,
                                 'src':data[index].thumbnail
	                      }).data('detailData',data[index]);
	                   }
	                   else
	                   {
	                   	 current.find('.front-flipper').attr({
                                 'data-id':data[index].id,
                                 'data-title':data[index].actual_img,
                                 'src':data[index].thumbnail
	                      }).data('detailData',data[index]);
	                   }
                  }
               	   
               	  current.find('a').toggleClass("start-flipper");
               }

               if(flag>=liLength-1)
               {
               	   flag=0;
               }
               else
               {
               	  flag++;
               }
                current=$minLi.eq(flag);
            	
		}, 5000);

		GC.addInterval(changeImg);
	},

	// actions: {
	// 	hideDetail: function() {
	// 		var self = this;
	// 		self.get('controller').set('showDetail', false);
	// 	}
	// },

	willDestroyElement: function() {
		GC.clearBinding();
		GC.clearIntervals();
	}
});