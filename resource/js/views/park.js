App.ParkView = Em.View.extend({
	didInsertElement: function() {
		var self = this;
		$(".r-page").removeClass("closepage").addClass("openpage");
		//选择车库的动画切换
		$('#p-choose a').click(function(){
			var $this = $(this);
			$(this).addClass('checked').siblings().removeClass('checked');
			$("#p-content").css({'position':'relative','height':' 453px'});
			$("#p-spot").css({'position':'absolute','top':'0','left':'1030px'});
			$("#p-choose").fadeOut();
		    $("#p-spot").animate({'left':0},function(){
		    	$(this).css({'left':0});
		    }).show();
		    if ($this.attr('data-name') === 'underground') {
		    	self.get('controller').set('isUndergroundLot', true);
		    	$(".sure font").css({display:"none"});
		    } else {
		    	self.get('controller').set('isUndergroundLot', false);
		    	$(".sure font").css({display:"block"});
		    }
		});

		$("#p-back").click(function(){
			$("#p-spot").fadeOut();
			$("#p-choose").animate({'left':1000},function(){
		    	$(this).css({'left':1000});
		    }).show(0);
		    $("#clearkey").click();
		});

		var codeNum="";
		var codeLength =3;
		$(".sure .code").mousedown(function(){
			if($(this).val()=="请输入停车位号"){
				$(this).val("");
			}
			// window.RuntimeTimer.update();
		});
		 $(".p-num ul li[t='n']").mousedown(function(){
			$(this).addClass('checked').siblings().removeClass('checked');
			//数字
		    if ($(".p-num ul li[t='n']").index($(this)) < 10){
		       if(codeNum.length<codeLength){
		       	 codeNum += $(this).text();
		       	 $(".sure .code").val(codeNum);
		       }
		    }
	    // window.RuntimeTimer.update();
		});
		 // 清空数据
		$("#clearkey").click(function(){
			codeNum = "";
			$(".p-num ul li[t='n']").removeClass('checked');
			$(".sure .code").val("请输入停车位号");
		});
		 // 减去最后一个
		$("#deletekey").click(function(){
			var code = $(".sure .code").val();
			codeNum = code.substr(0, code.length - 1);
			$(".sure .code").val(codeNum);
		});
	},
	actions: {
		search: function() {
			var self = this;
			var parkingNum = $.trim($('#parkingNum').val());
			this.get('controller').send('showParkNav', parkingNum);
		}
	}
});