App.ActivityController = Em.ArrayController.extend({

	firstClassModel: function() {
		return this.get('model').filterBy('priority', 1);
	}.property(),

	secondClassModel: function() {
		return this.get('model').filterBy('priority', 2);
	}.property(),

	actions: {
		gotoIndex: function() {
			this.set('showDetail', false);
			this.transitionToRoute('index');
		},
		hideDetail: function() {
			this.set('showDetail', false);
		}
	}
});