App.StoresController = Em.ArrayController.extend({

	store: null,

	showDetail: false,

	showPopMap: false,

	currentQueryType: 'category',

	currentQueryContent: '1',

	currentScrollTop: 400,

	isCategoryChanged: false,

	page: 1,

	actions: {
		loadMore: function() {
			var self = this;

			if (this.get('isCategoryChanged')) {
				this.set('page', 1);
			} else {
				this.set('page', this.get('page') + 1);
			}

			if(this.get('currentQueryContent')=='1'&&this.get('content_id')!=null&&this.get('content_id')!=''){
				this.set('currentQueryContent',this.get('content_id'))
			}
			
			console.log(this.get('currentQueryContent'));

			var url = _Const.url.getStores + '?callback=?&page=' + this.get('page') + '&type=' + this.get('currentQueryType') + '&content=' + this.get('currentQueryContent');

			console.log(url);

			$.getJSON(url).then(function(data) {
				//判断当前返回的数组有否为空
				if (!data.length) {
					return false;
				}
				if (self.get('isCategoryChanged')) {
					self.get('model').clear();
					self.set('model', data);
				} else {
					self.get('model').pushObjects(data);
				}
				self.set('isCategoryChanged', false);
			});
		},

		showDetailCom: function(store) {
			var self = this;
	/*		console.log('#########################');
			console.log(store);*/

			window.navStore = {store:store,option:_Const.popupOption.stores.detail};

			this.set('showAddress', false);
			this.set('showDetail', true);

			this.set('store', store);

			// this.set('storeDetailClass', 'Blur-box-fade-in');
		},
		showPopMap: function(store){
            //this.set('showPopMap', true);
            //this.set('store', store);

            $(".r-page").removeClass("openpage").addClass("closepage");
	        Utils.PrefixedEvent($(".r-page"), "AnimationEnd", function() {
	            App.Router.router.transitionTo('floor');
	          });
            
            window.navStore = {store:store,option:_Const.popupOption.floor.storeDetail};
		},
		hidePopMap: function(){
            this.set('showPopMap', false);
		},
		hideDetail: function() {
			this.set('showDetail', false);
			this.set('showAddress', false);
		},
		showAddress: function() {
			this.set('showDetail', false);
			this.set('showAddress', true);
		},
		gotoIndex: function() {
			this.set('showDetail', false);
			this.set('showAddress', false);
			this.set('page', 1);
			this.set('store', null);
			this.transitionToRoute('index');
		},
		categoryQuery: function(queryObj) {
			//如果发现查询的类别有变化，则重置页数，从第一页开始查询
			if (this.get('currentQueryType') != queryObj.type || this.get('currentQueryContent') != queryObj.content) {
				this.set('currentQueryType', queryObj.type);
				this.set('currentQueryContent', queryObj.content);
				this.set('isCategoryChanged', true);
				this.set('currentScrollTop', 400);
			}
			this.send('loadMore');
		}
	}
});