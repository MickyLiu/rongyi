App.ParkController = Em.Controller.extend({

	actions: {
		showParkNav: function(parkingNum) {
			var self = this;
			var url = _Const.url.getUndergroundParkInfo;
			var isUndergroundLot = this.get('isUndergroundLot');
			var park_info = _Const.text.park_instrunction_underground;
			if (!isUndergroundLot) {
				url = _Const.url.get3DParkInfo;
				park_info = _Const.text.park_instrunction_3d;
			}
			$.getJSON(url + parkingNum).then(function(data) {
				console.log(data);
				if ($.isEmptyObject(data)) {//查不到车位，返回空对象
					console.log(_Const.text.empty_park_msg);
					self.set('msg', _Const.text.empty_park_msg);
					self.set('isMsg', true);
				} else {
					console.log(data);
					console.log(park_info.replace('[area]', data.area));
					self.set('area', park_info.replace('[area]', data.area));
                    self.set('data',data);
					self.set('isMsg', false);
					self.set('showParkNav', true);
				}
			});
		},
		hideDetail: function() {
			$("#clearkey").click();
			this.set('showParkNav', false);
			this.set('isMsg', false);
		},

		gotoIndex: function() {
			this.set('showParkNav', false);
			this.set('isMsg', false);
			this.transitionToRoute('index');
		}
	}
});
