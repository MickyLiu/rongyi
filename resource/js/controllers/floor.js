App.FloorController = Em.ArrayController.extend({

	currentFloorIndex: 0,

	currentShop: null,

	showMapNav: false,

	showDetail: false,

	currentFloor: function() {
		return this.get('model').objectAt(this.get('currentFloorIndex'));
	}.property('currentFloorIndex'),

	actions: {
		setCurrentFloorIndex: function(i) {
			this.set('currentFloorIndex', i);
		},
		setPaper: function(paper) {
			console.log('in set paper');
			this.set('paper', paper);
		},
		gotoIndex: function() {
			this.set('showStoreDetail', false);
			this.transitionToRoute('index');
		},
		hideDetail: function() {
			var self = this;
			self.set('showStoreDetail', false);
		},
		showStoreDetail: function(shop) {
			var self = this;
			console.log('in showStoreDetail');
			console.log(shop);
			if(typeof shop=='undefined' || shop==null)
			{
				self.set('currentShop', window.navStore.store);
			    self.set('showStoreDetail', true);
			}
			else
			{
                self.set('currentShop', shop);
			    self.set('showStoreDetail', true);
			}
		}
	}

});