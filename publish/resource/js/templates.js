Ember.TEMPLATES["activity"] = Ember.Handlebars.template(function anonymous(Handlebars,depth0,helpers,partials,data) {
this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Ember.Handlebars.helpers); data = data || {};
  var buffer = '', stack1, helper, options, escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing, self=this;

function program1(depth0,data) {
  
  var buffer = '', helper, options;
  data.buffer.push("\r\n			<!--<div class=\"Blur-box \"></div>\r\n			<div class=\"Blur-box-main\">\r\n\r\n				<div class=\"activities-detail\">			\r\n						\r\n				</div>\r\n\r\n			</div>\r\n			<div class=\"d-but\">\r\n				<a href=\"javascript:void(0);\" class=\"back\" ");
  data.buffer.push(escapeExpression(helpers.action.call(depth0, "hideDetail", {hash:{
    'target': ("view")
  },hashTypes:{'target': "STRING"},hashContexts:{'target': depth0},contexts:[depth0],types:["STRING"],data:data})));
  data.buffer.push("></a>\r\n				<a href=\"javascript:void(0);\" class=\"back-index\" ");
  data.buffer.push(escapeExpression(helpers.action.call(depth0, "gotoIndex", {hash:{},hashTypes:{},hashContexts:{},contexts:[depth0],types:["STRING"],data:data})));
  data.buffer.push("></a>\r\n			</div>-->\r\n			");
  data.buffer.push(escapeExpression((helper = helpers['static-detail'] || (depth0 && depth0['static-detail']),options={hash:{
    'article': ("article"),
    'isActivity': (true),
    'hideDetail': ("hideDetail"),
    'gotoIndex': ("gotoIndex")
  },hashTypes:{'article': "ID",'isActivity': "BOOLEAN",'hideDetail': "STRING",'gotoIndex': "STRING"},hashContexts:{'article': depth0,'isActivity': depth0,'hideDetail': depth0,'gotoIndex': depth0},contexts:[],types:[],data:data},helper ? helper.call(depth0, options) : helperMissing.call(depth0, "static-detail", options))));
  data.buffer.push("\r\n		");
  return buffer;
  }

  data.buffer.push("<div class=\"wrap\">\r\n	<div class=\"r-page wrap-main\">\r\n		");
  stack1 = helpers['if'].call(depth0, "showDetail", {hash:{},hashTypes:{},hashContexts:{},inverse:self.noop,fn:self.program(1, program1, data),contexts:[depth0],types:["ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\r\n		");
  stack1 = helpers._triageMustache.call(depth0, "top-weather", {hash:{},hashTypes:{},hashContexts:{},contexts:[depth0],types:["ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\r\n		");
  data.buffer.push(escapeExpression((helper = helpers['nav-bar'] || (depth0 && depth0['nav-bar']),options={hash:{
    'title': ("activity")
  },hashTypes:{'title': "STRING"},hashContexts:{'title': depth0},contexts:[],types:[],data:data},helper ? helper.call(depth0, options) : helperMissing.call(depth0, "nav-bar", options))));
  data.buffer.push("\r\n		<!--主体内容start-->\r\n		<div class=\"activities\">\r\n			<ul data-name=\"upper\">\r\n				<li class=\"max right big\">\r\n				</li>\r\n				<li class=\"min floatR small\">\r\n				   <a href=\"javascript:void(0);\"></a>\r\n				</li>\r\n				<li class=\"min right small\">\r\n				   <a href=\"javascript:void(0);\"></a>\r\n				</li>\r\n				<li class=\"max floatR big\">\r\n				</li>\r\n			</ul>\r\n			<ul data-name=\"bottom\">\r\n				<li class=\"bottom right small\">\r\n				    <a href=\"javascript:void(0);\"></a>\r\n				</li>\r\n				<li class=\"bottom right small\">\r\n				   <a href=\"javascript:void(0);\"></a>\r\n				</li>\r\n				<li class=\"bottom floatR small\">\r\n				   <a href=\"javascript:void(0);\"></a>\r\n				</li>\r\n			</ul>\r\n		</div>\r\n		<!--主体内容end-->\r\n	</div>\r\n</div>\r\n");
  return buffer;
  
});

Ember.TEMPLATES["ad"] = Ember.Handlebars.template(function anonymous(Handlebars,depth0,helpers,partials,data) {
this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Ember.Handlebars.helpers); data = data || {};
  var buffer = '', stack1;


  data.buffer.push("<div class=\"wrap\">\r\n");
  stack1 = helpers._triageMustache.call(depth0, "ad-rotation", {hash:{},hashTypes:{},hashContexts:{},contexts:[depth0],types:["ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\r\n</div>\r\n");
  return buffer;
  
});

Ember.TEMPLATES["components/ad-rotation"] = Ember.Handlebars.template(function anonymous(Handlebars,depth0,helpers,partials,data) {
this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Ember.Handlebars.helpers); data = data || {};
  var buffer = '', escapeExpression=this.escapeExpression;


  data.buffer.push("<div class=\"wrap-main r-page\">\r\n    <a id=\"outAd\" ");
  data.buffer.push(escapeExpression(helpers.action.call(depth0, "backToIndex", {hash:{},hashTypes:{},hashContexts:{},contexts:[depth0],types:["STRING"],data:data})));
  data.buffer.push(">退出广告</a>\r\n	<div class=\"ad\">\r\n	     \r\n	</div>\r\n</div>");
  return buffer;
  
});

Ember.TEMPLATES["components/bottom-lead"] = Ember.Handlebars.template(function anonymous(Handlebars,depth0,helpers,partials,data) {
this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Ember.Handlebars.helpers); data = data || {};
  var buffer = '', stack1, escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = '', stack1;
  data.buffer.push("\r\n\r\n			<div class=\"nav_lead\" ");
  data.buffer.push(escapeExpression(helpers['bind-attr'].call(depth0, {hash:{
    'data-content': ("list.id")
  },hashTypes:{'data-content': "ID"},hashContexts:{'data-content': depth0},contexts:[],types:[],data:data})));
  data.buffer.push(">\r\n				  <img/>\r\n			      <div class=\"\">\r\n			      <img/>\r\n			      <span>");
  stack1 = helpers._triageMustache.call(depth0, "list.name", {hash:{},hashTypes:{},hashContexts:{},contexts:[depth0],types:["ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("</span>\r\n			      <span>");
  stack1 = helpers._triageMustache.call(depth0, "list.alias_name", {hash:{},hashTypes:{},hashContexts:{},contexts:[depth0],types:["ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("</span>\r\n			      </div>\r\n		    </div>\r\n\r\n		");
  return buffer;
  }

  data.buffer.push("<div id=\"bottom_lead\" class=\"wrap\" >\r\n	<div class=\"nav_wrap bottom_wrap\">\r\n\r\n		");
  stack1 = helpers.each.call(depth0, "list", "in", "leads", {hash:{},hashTypes:{},hashContexts:{},inverse:self.noop,fn:self.program(1, program1, data),contexts:[depth0,depth0,depth0],types:["ID","ID","ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\r\n		\r\n	</div>\r\n</div>");
  return buffer;
  
});

Ember.TEMPLATES["components/bottom-map"] = Ember.Handlebars.template(function anonymous(Handlebars,depth0,helpers,partials,data) {
this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Ember.Handlebars.helpers); data = data || {};
  


  data.buffer.push("<div class=\"wrap\" style=\"height:690px;margin-top:33px;\">\r\n	<div class=\"map_wrap\">\r\n		<div class=\"index-map\" id=\"mapDiv\">\r\n			<div class=\"compass\"></div>\r\n			<div class=\"map_title\"></div>\r\n		</div>\r\n	</div>\r\n</div>");
  
});

Ember.TEMPLATES["components/category-list"] = Ember.Handlebars.template(function anonymous(Handlebars,depth0,helpers,partials,data) {
this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Ember.Handlebars.helpers); data = data || {};
  var buffer = '', stack1, escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = '', stack1;
  data.buffer.push("\r\n			<li>\r\n				<a href=\"javascript:void(0)\" ");
  data.buffer.push(escapeExpression(helpers['bind-attr'].call(depth0, {hash:{
    'data-content': ("list.f_cate.id")
  },hashTypes:{'data-content': "ID"},hashContexts:{'data-content': depth0},contexts:[],types:[],data:data})));
  data.buffer.push(" ");
  data.buffer.push(escapeExpression(helpers['bind-attr'].call(depth0, {hash:{
    'class': ("list._index")
  },hashTypes:{'class': "ID"},hashContexts:{'class': depth0},contexts:[],types:[],data:data})));
  data.buffer.push("><img /><span>");
  stack1 = helpers._triageMustache.call(depth0, "list.f_cate.name", {hash:{},hashTypes:{},hashContexts:{},contexts:[depth0],types:["ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("</span></a>\r\n				<div class=\"nav-box\">\r\n					<ul>\r\n					    ");
  stack1 = helpers['if'].call(depth0, "list.s_cates", {hash:{},hashTypes:{},hashContexts:{},inverse:self.noop,fn:self.program(2, program2, data),contexts:[depth0],types:["ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\r\n						");
  stack1 = helpers.each.call(depth0, "item", "in", "list.s_cates", {hash:{},hashTypes:{},hashContexts:{},inverse:self.noop,fn:self.program(4, program4, data),contexts:[depth0,depth0,depth0],types:["ID","ID","ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\r\n					</ul>\r\n				</div>\r\n			</li>\r\n		");
  return buffer;
  }
function program2(depth0,data) {
  
  var buffer = '';
  data.buffer.push("\r\n					    <li><a href=\"javascript:void(0)\" ");
  data.buffer.push(escapeExpression(helpers['bind-attr'].call(depth0, {hash:{
    'data-content': ("list.f_cate.id")
  },hashTypes:{'data-content': "ID"},hashContexts:{'data-content': depth0},contexts:[],types:[],data:data})));
  data.buffer.push(">全部</a></li>\r\n					    ");
  return buffer;
  }

function program4(depth0,data) {
  
  var buffer = '', stack1;
  data.buffer.push("\r\n							<li><a href=\"javascript:void(0)\" ");
  data.buffer.push(escapeExpression(helpers['bind-attr'].call(depth0, {hash:{
    'data-content': ("item.id")
  },hashTypes:{'data-content': "ID"},hashContexts:{'data-content': depth0},contexts:[],types:[],data:data})));
  data.buffer.push(">");
  stack1 = helpers._triageMustache.call(depth0, "item.name", {hash:{},hashTypes:{},hashContexts:{},contexts:[depth0],types:["ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("</a></li>							\r\n						");
  return buffer;
  }

  data.buffer.push("<div class=\"con-left-T checked\">品类筛选</div>\r\n<div class=\"con-left-list\" style=\"display: block;\">\r\n	<ul class=\"categoryList\">\r\n		");
  stack1 = helpers.each.call(depth0, "list", "in", "lists", {hash:{},hashTypes:{},hashContexts:{},inverse:self.noop,fn:self.program(1, program1, data),contexts:[depth0,depth0,depth0],types:["ID","ID","ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\r\n	</ul>\r\n</div>\r\n<div class=\"con-left-T\">首字母筛选</div>\r\n<div class=\"con-left-list\">\r\n	<ul class=\"letter\">\r\n		<li class=\"max\"><a class=\"bottomNode alph\" data-value=\"all\" href=\"javascript:void(0)\">全部</a></li>\r\n		<li class=\"max\"><a data-value=\"number\" href=\"javascript:void(0)\">0~9</a></li>\r\n		<li><a data-value=\"a\" href=\"javascript:void(0)\">A</a></li>\r\n		<li><a data-value=\"b\" href=\"javascript:void(0)\">B</a></li>\r\n		<li><a data-value=\"c\" href=\"javascript:void(0)\">C</a></li>\r\n		<li><a data-value=\"d\" href=\"javascript:void(0)\">D</a></li>\r\n		<li><a data-value=\"e\" href=\"javascript:void(0)\">E</a></li>\r\n		<li><a data-value=\"f\" href=\"javascript:void(0)\">F</a></li>\r\n		<li><a data-value=\"g\" href=\"javascript:void(0)\">G</a></li>\r\n		<li><a data-value=\"h\" href=\"javascript:void(0)\">H</a></li>\r\n		<li><a data-value=\"i\" href=\"javascript:void(0)\">I</a></li>\r\n		<li><a data-value=\"j\" href=\"javascript:void(0)\">J</a></li>\r\n		<li><a data-value=\"k\" href=\"javascript:void(0)\">K</a></li>\r\n		<li><a data-value=\"l\" href=\"javascript:void(0)\">L</a></li>\r\n		<li><a data-value=\"m\" href=\"javascript:void(0)\">M</a></li>\r\n		<li><a data-value=\"n\" href=\"javascript:void(0)\">N</a></li>\r\n		<li><a data-value=\"o\" href=\"javascript:void(0)\">O</a></li>\r\n		<li><a data-value=\"p\" href=\"javascript:void(0)\">P</a></li>\r\n		<li><a data-value=\"q\" href=\"javascript:void(0)\">Q</a></li>\r\n		<li><a data-value=\"r\" href=\"javascript:void(0)\">R</a></li>\r\n		<li><a data-value=\"s\" href=\"javascript:void(0)\">S</a></li>\r\n		<li><a data-value=\"t\" href=\"javascript:void(0)\">T</a></li>\r\n		<li><a data-value=\"u\" href=\"javascript:void(0)\">U</a></li>\r\n		<li><a data-value=\"v\" href=\"javascript:void(0)\">V</a></li>\r\n		<li><a data-value=\"w\" href=\"javascript:void(0)\">W</a></li>\r\n		<li><a data-value=\"x\" href=\"javascript:void(0)\">X</a></li>\r\n		<li><a data-value=\"y\" href=\"javascript:void(0)\">Y</a></li>\r\n		<li><a data-value=\"z\" href=\"javascript:void(0)\">Z</a></li>\r\n	</ul>\r\n</div>\r\n<div class=\"con-left-T bottomNode isFavored\">优惠筛选</div>\r\n<!--<div class=\"con-left-T bottomNode address\">门牌号</div>-->");
  return buffer;
  
});

Ember.TEMPLATES["components/item-overview"] = Ember.Handlebars.template(function anonymous(Handlebars,depth0,helpers,partials,data) {
this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Ember.Handlebars.helpers); data = data || {};
  var buffer = '', stack1, escapeExpression=this.escapeExpression;


  data.buffer.push("<div class=\"con-right-list\">\r\n	<div class=\"pic\">\r\n		<img ");
  data.buffer.push(escapeExpression(helpers['bind-attr'].call(depth0, {hash:{
    'src': ("store.img")
  },hashTypes:{'src': "ID"},hashContexts:{'src': depth0},contexts:[],types:[],data:data})));
  data.buffer.push(" />\r\n	</div>\r\n	<div class=\"intro\">\r\n		<div class=\"title\">\r\n			<img ");
  data.buffer.push(escapeExpression(helpers['bind-attr'].call(depth0, {hash:{
    'src': ("store.icon")
  },hashTypes:{'src': "ID"},hashContexts:{'src': depth0},contexts:[],types:[],data:data})));
  data.buffer.push("/>\r\n			<span>");
  stack1 = helpers._triageMustache.call(depth0, "store.name", {hash:{},hashTypes:{},hashContexts:{},contexts:[depth0],types:["ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("</span>\r\n		</div>\r\n		<div class=\"cost\">\r\n			<span>");
  stack1 = helpers._triageMustache.call(depth0, "store.tag", {hash:{},hashTypes:{},hashContexts:{},contexts:[depth0],types:["ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("</span>\r\n			<span><p>");
  stack1 = helpers._triageMustache.call(depth0, "store.floor", {hash:{},hashTypes:{},hashContexts:{},contexts:[depth0],types:["ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("</p> <em>");
  stack1 = helpers._triageMustache.call(depth0, "store.locationCode", {hash:{},hashTypes:{},hashContexts:{},contexts:[depth0],types:["ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("</em></span>\r\n		</div>\r\n		<div class=\"button\">\r\n		    <a href=\"javscript:void(0);\" ");
  data.buffer.push(escapeExpression(helpers.action.call(depth0, "showPopMap", "store", {hash:{},hashTypes:{},hashContexts:{},contexts:[depth0,depth0],types:["STRING","ID"],data:data})));
  data.buffer.push("></a>	\r\n			<a href=\"javscript:void(0);\" ");
  data.buffer.push(escapeExpression(helpers.action.call(depth0, "showDetailCom", "store", {hash:{},hashTypes:{},hashContexts:{},contexts:[depth0,depth0],types:["STRING","ID"],data:data})));
  data.buffer.push("></a>\r\n		</div>\r\n	</div>\r\n</div>");
  return buffer;
  
});

Ember.TEMPLATES["components/nav-bar"] = Ember.Handlebars.template(function anonymous(Handlebars,depth0,helpers,partials,data) {
this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Ember.Handlebars.helpers); data = data || {};
  var buffer = '', stack1, escapeExpression=this.escapeExpression;


  data.buffer.push("<div class=\"nav\">\r\n	<div class=\"nav-left\">\r\n		<p ");
  data.buffer.push(escapeExpression(helpers['bind-attr'].call(depth0, {hash:{
    'class': ("className")
  },hashTypes:{'class': "ID"},hashContexts:{'class': depth0},contexts:[],types:[],data:data})));
  data.buffer.push("></p>\r\n		<span>");
  stack1 = helpers._triageMustache.call(depth0, "title", {hash:{},hashTypes:{},hashContexts:{},contexts:[depth0],types:["ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("</span>\r\n	</div>\r\n	<div class=\"nav-right\" ");
  data.buffer.push(escapeExpression(helpers.action.call(depth0, "backToIndex", {hash:{},hashTypes:{},hashContexts:{},contexts:[depth0],types:["STRING"],data:data})));
  data.buffer.push("></div>\r\n</div>");
  return buffer;
  
});

Ember.TEMPLATES["components/nav-blocks"] = Ember.Handlebars.template(function anonymous(Handlebars,depth0,helpers,partials,data) {
this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Ember.Handlebars.helpers); data = data || {};
  


  data.buffer.push("<div class=\"wrap\">\r\n	<div class=\"nav_wrap\">\r\n		<!--第一个活动大图开始-->\r\n		<div class=\"nav_cont nav0\"  data-route=\"activity\" data-title=\"6\">\r\n			<div></div>\r\n		</div>\r\n		<!--第一个活动大图结束-->\r\n		\r\n		<!--品牌导购开始-->\r\n		<div class=\"nav_cont nav3\" data-route=\"stores\" data-title=\"all\">\r\n			<div></div>\r\n		</div>\r\n		<!--品牌导购结束-->\r\n		\r\n		<!--会员中心开始-->\r\n		<div class=\"nav_cont nav6\" data-route=\"static\" data-title=\"member\">\r\n			<div></div>\r\n		</div>\r\n		<!--会员中心结束-->\r\n\r\n		<!--楼栋导航开始-->\r\n		<div class=\"nav_cont nav10\" data-route=\"floor\">\r\n			<div></div>\r\n		</div>\r\n		<!--楼栋导航结束-->\r\n		<!--二维码开始-->\r\n		<div class=\"nav_cont nav4\">\r\n			<div></div>\r\n		</div>\r\n		<!--二维码结束-->\r\n\r\n		<!--精彩活动开始-->\r\n		<div class=\"nav_cont nav7\" data-route=\"activity\" data-title=\"all\">\r\n			<div></div>\r\n		</div>\r\n		<!--精彩活动结束-->\r\n\r\n		<!--停车位开始-->\r\n		<div class=\"nav_cont nav2\" data-route=\"floor\">\r\n			<div></div>\r\n		</div>\r\n		<!--停车位结束-->\r\n		<!--商场介绍开始-->\r\n		<div class=\"nav_cont nav1\" data-route=\"static\" data-title=\"mall_info\">\r\n			<div></div>\r\n		</div>\r\n		<!--商场介绍结束-->\r\n\r\n	</div>\r\n</div>\r\n");
  
});

Ember.TEMPLATES["components/option-button"] = Ember.Handlebars.template(function anonymous(Handlebars,depth0,helpers,partials,data) {
this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Ember.Handlebars.helpers); data = data || {};
  var buffer = '', stack1, escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = '';
  data.buffer.push("\r\n<a class=\"back\" ");
  data.buffer.push(escapeExpression(helpers.action.call(depth0, "hideDetail", {hash:{
    'target': ("view")
  },hashTypes:{'target': "STRING"},hashContexts:{'target': depth0},contexts:[depth0],types:["STRING"],data:data})));
  data.buffer.push("></a>\r\n<a href=\"#\" class=\"back-index\" ");
  data.buffer.push(escapeExpression(helpers.action.call(depth0, "gotoIndex", {hash:{},hashTypes:{},hashContexts:{},contexts:[depth0],types:["STRING"],data:data})));
  data.buffer.push("></a>\r\n<a class=\"menu\"></a> \r\n");
  return buffer;
  }

  data.buffer.push("\r\n<div id=\"test\">");
  stack1 = helpers._triageMustache.call(depth0, "buttonOptions.buttonCount", {hash:{},hashTypes:{},hashContexts:{},contexts:[depth0],types:["ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("</div>\r\n\r\n\r\n<a class=\"back\" ");
  data.buffer.push(escapeExpression(helpers.action.call(depth0, "hideDetail", {hash:{
    'target': ("view")
  },hashTypes:{'target': "STRING"},hashContexts:{'target': depth0},contexts:[depth0],types:["STRING"],data:data})));
  data.buffer.push("></a>\r\n<a href=\"#\" class=\"back-index\" ");
  data.buffer.push(escapeExpression(helpers.action.call(depth0, "gotoIndex", {hash:{},hashTypes:{},hashContexts:{},contexts:[depth0],types:["STRING"],data:data})));
  data.buffer.push("></a>\r\n\r\n\r\n");
  stack1 = helpers['if'].call(depth0, "option.buttonType_1", {hash:{},hashTypes:{},hashContexts:{},inverse:self.noop,fn:self.program(1, program1, data),contexts:[depth0],types:["ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\r\n\r\n\r\n\r\n");
  return buffer;
  
});

Ember.TEMPLATES["components/park-nav"] = Ember.Handlebars.template(function anonymous(Handlebars,depth0,helpers,partials,data) {
this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Ember.Handlebars.helpers); data = data || {};
  var buffer = '', stack1, escapeExpression=this.escapeExpression;


  data.buffer.push("<div class=\"Blur-box \"></div>\r\n<div class=\"Blur-box-main\">\r\n	<div class=\"p-detail-top\">\r\n		<div class=\"p-detail-top-left\"></div>\r\n		<div class=\"p-detail-top-line\"></div>\r\n		<div class=\"p-detail-top-right\">\r\n			<span>路线指引</span>\r\n			<p name=\"instruction_underground\">您的车位在");
  stack1 = helpers._triageMustache.call(depth0, "area", {hash:{},hashTypes:{},hashContexts:{},contexts:[depth0],types:["ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("</p>\r\n		</div>\r\n	</div>\r\n	<div class=\"detail\">\r\n		<div class=\"p-detail-map\">\r\n			<span>广场地图</span>\r\n			<div id=\"squareMap\"></div>\r\n		</div>\r\n		<div class=\"p-detail-map\">\r\n			<span>车库外景图</span>\r\n			<div id=\"undergroundMap\"></div>\r\n		</div>\r\n	</div>\r\n\r\n</div>\r\n<div class=\"d-but\">\r\n	<a href=\"javascript:void(0);\" class=\"back\" ");
  data.buffer.push(escapeExpression(helpers.action.call(depth0, "hideDetail", {hash:{},hashTypes:{},hashContexts:{},contexts:[depth0],types:["STRING"],data:data})));
  data.buffer.push("></a>\r\n	<a href=\"javascript:void(0);\" class=\"back-index\" ");
  data.buffer.push(escapeExpression(helpers.action.call(depth0, "gotoIndex", {hash:{},hashTypes:{},hashContexts:{},contexts:[depth0],types:["STRING"],data:data})));
  data.buffer.push("></a>\r\n</div>");
  return buffer;
  
});

Ember.TEMPLATES["components/popup-map"] = Ember.Handlebars.template(function anonymous(Handlebars,depth0,helpers,partials,data) {
this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Ember.Handlebars.helpers); data = data || {};
  var buffer = '', stack1, escapeExpression=this.escapeExpression;


  data.buffer.push("\r\n	<div ");
  data.buffer.push(escapeExpression(helpers['bind-attr'].call(depth0, {hash:{
    'class': (":Blur-box storeDetailClass")
  },hashTypes:{'class': "STRING"},hashContexts:{'class': depth0},contexts:[],types:[],data:data})));
  data.buffer.push("></div> \r\n	<div ");
  data.buffer.push(escapeExpression(helpers['bind-attr'].call(depth0, {hash:{
    'class': (":Blur-box-main storeDetailClass")
  },hashTypes:{'class': "STRING"},hashContexts:{'class': depth0},contexts:[],types:[],data:data})));
  data.buffer.push(">\r\n		<div class=\"detail\">\r\n			<div class=\"d-logo\">\r\n				<div class=\"d-logoL\">\r\n					<div class=\"pic\"><img ");
  data.buffer.push(escapeExpression(helpers['bind-attr'].call(depth0, {hash:{
    'src': ("store.icon")
  },hashTypes:{'src': "ID"},hashContexts:{'src': depth0},contexts:[],types:[],data:data})));
  data.buffer.push("/></div>\r\n					<span>\r\n					<em>");
  stack1 = helpers._triageMustache.call(depth0, "store.name", {hash:{},hashTypes:{},hashContexts:{},contexts:[depth0],types:["ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("</em>\r\n					<hr/>\r\n					<span>");
  stack1 = helpers._triageMustache.call(depth0, "store.locationCode", {hash:{},hashTypes:{},hashContexts:{},contexts:[depth0],types:["ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("</span>\r\n					<font>");
  stack1 = helpers._triageMustache.call(depth0, "store.floor", {hash:{},hashTypes:{},hashContexts:{},contexts:[depth0],types:["ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("</font>\r\n					</span>\r\n				</div>\r\n			</div>\r\n			<div class=\"d-map\" id=\"d_map\" style=\"margin-top:40px;\">\r\n				<div class=\"d-map-compass\"></div>\r\n				<!--<img src=\"resource/images/map.jpg\"/>-->\r\n			</div>\r\n			<div class=\"d-map-text\">*您要去的店铺在<em>");
  stack1 = helpers._triageMustache.call(depth0, "store.floor", {hash:{},hashTypes:{},hashContexts:{},contexts:[depth0],types:["ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("</em><em>");
  stack1 = helpers._triageMustache.call(depth0, "store.locationCode", {hash:{},hashTypes:{},hashContexts:{},contexts:[depth0],types:["ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("</em></div>\r\n		</div>\r\n\r\n	</div>\r\n	<div ");
  data.buffer.push(escapeExpression(helpers['bind-attr'].call(depth0, {hash:{
    'class': (":d-but storeDetailClass")
  },hashTypes:{'class': "STRING"},hashContexts:{'class': depth0},contexts:[],types:[],data:data})));
  data.buffer.push(">\r\n		<a id=\"Blurbackclose\" class=\"back\" ");
  data.buffer.push(escapeExpression(helpers.action.call(depth0, "hidePopMap", {hash:{},hashTypes:{},hashContexts:{},contexts:[depth0],types:["STRING"],data:data})));
  data.buffer.push("></a>\r\n		<a href=\"#\" class=\"back-index\" ");
  data.buffer.push(escapeExpression(helpers.action.call(depth0, "gotoIndex", {hash:{},hashTypes:{},hashContexts:{},contexts:[depth0],types:["STRING"],data:data})));
  data.buffer.push("></a>\r\n		<!-- <a class=\"menu\"></a> -->\r\n	</div> \r\n");
  return buffer;
  
});

Ember.TEMPLATES["components/static-detail"] = Ember.Handlebars.template(function anonymous(Handlebars,depth0,helpers,partials,data) {
this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Ember.Handlebars.helpers); data = data || {};
  var buffer = '', stack1, escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = '', stack1;
  data.buffer.push("\r\n		<div class=\"activities-detail\">\r\n		    <div class=\"activity_title\">\r\n				<p></p>\r\n				<span>活动详情</span>\r\n	        </div>			\r\n				<a href=\"javascript:void(0);\">\r\n					<img class=\"activity_detail\" ");
  data.buffer.push(escapeExpression(helpers['bind-attr'].call(depth0, {hash:{
    'src': ("article.src")
  },hashTypes:{'src': "ID"},hashContexts:{'src': depth0},contexts:[],types:[],data:data})));
  data.buffer.push(" />\r\n				</a>\r\n				<p>活动标题：<span>");
  stack1 = helpers._triageMustache.call(depth0, "article.title", {hash:{},hashTypes:{},hashContexts:{},contexts:[depth0],types:["ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("</span></p>\r\n				<p>活动时间：<span>");
  stack1 = helpers._triageMustache.call(depth0, "article.text", {hash:{},hashTypes:{},hashContexts:{},contexts:[depth0],types:["ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("</span></p>\r\n				<p>活动地点：<span>");
  stack1 = helpers._triageMustache.call(depth0, "article.text", {hash:{},hashTypes:{},hashContexts:{},contexts:[depth0],types:["ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("</span></p>\r\n				<p>活动内容：<span>");
  stack1 = helpers._triageMustache.call(depth0, "article.text", {hash:{},hashTypes:{},hashContexts:{},contexts:[depth0],types:["ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("</span></p>\r\n		</div>\r\n	");
  return buffer;
  }

function program3(depth0,data) {
  
  var buffer = '';
  data.buffer.push("\r\n		<div class=\"p-detail-top\">\r\n			<div class=\"p-detail-top-left house-number\"></div>\r\n			<div class=\"p-detail-top-line\"></div>\r\n			<div class=\"p-detail-top-right\">\r\n				<span>门牌号</span>\r\n				<p>请根据楼座查询门牌号</p>\r\n			</div>\r\n		</div>\r\n		<div class=\"detail\">\r\n			<div class=\"p-detail-map p-detail-map-nav p-detail-map-height\">\r\n				<img ");
  data.buffer.push(escapeExpression(helpers['bind-attr'].call(depth0, {hash:{
    'src': ("article.src")
  },hashTypes:{'src': "ID"},hashContexts:{'src': depth0},contexts:[],types:[],data:data})));
  data.buffer.push(" />\r\n			</div>\r\n		</div>\r\n	");
  return buffer;
  }

function program5(depth0,data) {
  
  var buffer = '', stack1;
  data.buffer.push("\r\n		<div class=\"p-detail-top\">\r\n			<div class=\"p-detail-top-right\">\r\n				<span>提示</span>\r\n			</div>\r\n		</div>\r\n		<div class=\"detail\">\r\n			<div class=\"p-detail-map p-detail-map-nav\">\r\n				<span>");
  stack1 = helpers._triageMustache.call(depth0, "msg", {hash:{},hashTypes:{},hashContexts:{},contexts:[depth0],types:["ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("</span>\r\n			</div>\r\n		</div>\r\n	");
  return buffer;
  }

function program7(depth0,data) {
  
  var buffer = '';
  data.buffer.push("\r\n					<a class=\"back\" ");
  data.buffer.push(escapeExpression(helpers.action.call(depth0, "hideDetail", {hash:{
    'target': ("view")
  },hashTypes:{'target': "STRING"},hashContexts:{'target': depth0},contexts:[depth0],types:["STRING"],data:data})));
  data.buffer.push("></a>\r\n					<a href=\"#\" class=\"back-index\" ");
  data.buffer.push(escapeExpression(helpers.action.call(depth0, "gotoIndex", {hash:{},hashTypes:{},hashContexts:{},contexts:[depth0],types:["STRING"],data:data})));
  data.buffer.push("></a>\r\n				");
  return buffer;
  }

function program9(depth0,data) {
  
  var buffer = '';
  data.buffer.push("\r\n					<a class=\"back\" ");
  data.buffer.push(escapeExpression(helpers.action.call(depth0, "hideDetail", {hash:{
    'target': ("view")
  },hashTypes:{'target': "STRING"},hashContexts:{'target': depth0},contexts:[depth0],types:["STRING"],data:data})));
  data.buffer.push("></a>\r\n					<!--1111111111-->\r\n					<a href=\"#\" class=\"back-index\" ");
  data.buffer.push(escapeExpression(helpers.action.call(depth0, "gotoIndex", {hash:{},hashTypes:{},hashContexts:{},contexts:[depth0],types:["STRING"],data:data})));
  data.buffer.push("></a>\r\n					<a class=\"menu\"></a>\r\n				");
  return buffer;
  }

  data.buffer.push("<div class=\"Blur-box\"></div>\r\n<div class=\"Blur-box-main\">\r\n\r\n\r\n\r\n	");
  stack1 = helpers['if'].call(depth0, "isActivity", {hash:{},hashTypes:{},hashContexts:{},inverse:self.noop,fn:self.program(1, program1, data),contexts:[depth0],types:["ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\r\n\r\n	");
  stack1 = helpers['if'].call(depth0, "isAddressMap", {hash:{},hashTypes:{},hashContexts:{},inverse:self.noop,fn:self.program(3, program3, data),contexts:[depth0],types:["ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\r\n\r\n	");
  stack1 = helpers['if'].call(depth0, "isMsg", {hash:{},hashTypes:{},hashContexts:{},inverse:self.noop,fn:self.program(5, program5, data),contexts:[depth0],types:["ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\r\n\r\n</div>\r\n<div class=\"d-but\">\r\n                ");
  stack1 = helpers['if'].call(depth0, "buttonOption.type_1", {hash:{},hashTypes:{},hashContexts:{},inverse:self.noop,fn:self.program(7, program7, data),contexts:[depth0],types:["ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\r\n\r\n				");
  stack1 = helpers['if'].call(depth0, "buttonOption.type_0", {hash:{},hashTypes:{},hashContexts:{},inverse:self.noop,fn:self.program(9, program9, data),contexts:[depth0],types:["ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\r\n</div>");
  return buffer;
  
});

Ember.TEMPLATES["components/store-detail"] = Ember.Handlebars.template(function anonymous(Handlebars,depth0,helpers,partials,data) {
this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Ember.Handlebars.helpers); data = data || {};
  var buffer = '', stack1, escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = '';
  data.buffer.push("\r\n								<li class=\"imgSlider\"><img ");
  data.buffer.push(escapeExpression(helpers['bind-attr'].call(depth0, {hash:{
    'src': ("i.src")
  },hashTypes:{'src': "ID"},hashContexts:{'src': depth0},contexts:[],types:[],data:data})));
  data.buffer.push(" /></li>\r\n							");
  return buffer;
  }

function program3(depth0,data) {
  
  var buffer = '';
  data.buffer.push("\r\n			<a class=\"back\" ");
  data.buffer.push(escapeExpression(helpers.action.call(depth0, "hideDetail", {hash:{
    'target': ("view")
  },hashTypes:{'target': "STRING"},hashContexts:{'target': depth0},contexts:[depth0],types:["STRING"],data:data})));
  data.buffer.push("></a>\r\n			<a href=\"#\" class=\"back-index\" ");
  data.buffer.push(escapeExpression(helpers.action.call(depth0, "gotoIndex", {hash:{},hashTypes:{},hashContexts:{},contexts:[depth0],types:["STRING"],data:data})));
  data.buffer.push("></a>\r\n		");
  return buffer;
  }

function program5(depth0,data) {
  
  var buffer = '';
  data.buffer.push("\r\n			<a class=\"back\" ");
  data.buffer.push(escapeExpression(helpers.action.call(depth0, "hideDetail", {hash:{
    'target': ("view")
  },hashTypes:{'target': "STRING"},hashContexts:{'target': depth0},contexts:[depth0],types:["STRING"],data:data})));
  data.buffer.push("></a>\r\n			<!--1111111111-->\r\n			<a href=\"#\" class=\"back-index\" ");
  data.buffer.push(escapeExpression(helpers.action.call(depth0, "gotoIndex", {hash:{},hashTypes:{},hashContexts:{},contexts:[depth0],types:["STRING"],data:data})));
  data.buffer.push("></a>\r\n			<a class=\"menu\"></a>\r\n		");
  return buffer;
  }

  data.buffer.push("\r\n	<div ");
  data.buffer.push(escapeExpression(helpers['bind-attr'].call(depth0, {hash:{
    'class': (":Blur-box storeDetailClass")
  },hashTypes:{'class': "STRING"},hashContexts:{'class': depth0},contexts:[],types:[],data:data})));
  data.buffer.push("></div> \r\n	<div ");
  data.buffer.push(escapeExpression(helpers['bind-attr'].call(depth0, {hash:{
    'class': (":Blur-box-main storeDetailClass")
  },hashTypes:{'class': "STRING"},hashContexts:{'class': depth0},contexts:[],types:[],data:data})));
  data.buffer.push(">\r\n		<div class=\"detail\">\r\n			<div class=\"d-logo\">\r\n				<div class=\"d-logoL\">\r\n					<div class=\"pic\"><img ");
  data.buffer.push(escapeExpression(helpers['bind-attr'].call(depth0, {hash:{
    'src': ("store.icon")
  },hashTypes:{'src': "ID"},hashContexts:{'src': depth0},contexts:[],types:[],data:data})));
  data.buffer.push("/></div>\r\n					<span>\r\n					<em>");
  stack1 = helpers._triageMustache.call(depth0, "store.name", {hash:{},hashTypes:{},hashContexts:{},contexts:[depth0],types:["ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("</em>\r\n					<hr/>\r\n					<span>");
  stack1 = helpers._triageMustache.call(depth0, "store.locationCode", {hash:{},hashTypes:{},hashContexts:{},contexts:[depth0],types:["ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("</span>\r\n					<font>");
  stack1 = helpers._triageMustache.call(depth0, "store.floor", {hash:{},hashTypes:{},hashContexts:{},contexts:[depth0],types:["ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("</font>\r\n					</span>\r\n				</div>\r\n			</div>\r\n			<div class=\"d-pic\">\r\n				<div class=\"d-pic-img\">\r\n					<div class=\"d-pic-img-area\">\r\n						<ul>\r\n							");
  stack1 = helpers.each.call(depth0, "i", "in", "store.actualImg", {hash:{},hashTypes:{},hashContexts:{},inverse:self.noop,fn:self.program(1, program1, data),contexts:[depth0,depth0,depth0],types:["ID","ID","ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\r\n						</ul>\r\n					</div>\r\n				</div>\r\n				<p>");
  stack1 = helpers._triageMustache.call(depth0, "store.desc", {hash:{},hashTypes:{},hashContexts:{},contexts:[depth0],types:["ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("</p>\r\n			</div>\r\n			\r\n			<div class=\"d-map-text\">\r\n				<div  class=\"d-map-text_1\">\r\n				</div>\r\n				<em>70抵100</em>\r\n				<em>限量1000份</em>\r\n				<div class=\"d-map-text_2\">\r\n				</div>\r\n			</div>\r\n\r\n			<!--<div class=\"d-map-compass\">\r\n\r\n			</div>-->\r\n		</div>\r\n\r\n	</div>\r\n	<div ");
  data.buffer.push(escapeExpression(helpers['bind-attr'].call(depth0, {hash:{
    'class': (":d-but storeDetailClass")
  },hashTypes:{'class': "STRING"},hashContexts:{'class': depth0},contexts:[],types:[],data:data})));
  data.buffer.push(">\r\n \r\n		");
  stack1 = helpers['if'].call(depth0, "buttonOption.type_1", {hash:{},hashTypes:{},hashContexts:{},inverse:self.noop,fn:self.program(3, program3, data),contexts:[depth0],types:["ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\r\n\r\n		");
  stack1 = helpers['if'].call(depth0, "buttonOption.type_0", {hash:{},hashTypes:{},hashContexts:{},inverse:self.noop,fn:self.program(5, program5, data),contexts:[depth0],types:["ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\r\n	</div> \r\n\r\n\r\n\r\n");
  return buffer;
  
});

Ember.TEMPLATES["components/top-weather"] = Ember.Handlebars.template(function anonymous(Handlebars,depth0,helpers,partials,data) {
this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Ember.Handlebars.helpers); data = data || {};
  


  data.buffer.push("<div class=\"wrap header\">\r\n	<div class=\"top_wrap\">\r\n		<div class=\"head\">\r\n		   <div class=\"top_title\">\r\n			    <span class=\"location\">上海 </span>\r\n			    <span class=\"currentDate\">2014-07-23</span>\r\n			</div>\r\n			<div style=\"position:absolute;top:0;z-index:10000\">\r\n			\r\n				<div class=\"clock\">\r\n				  <svg id=\"myclock\" class=\"clockCanvas\">\r\n					 \r\n				  </svg>\r\n				</div>\r\n				<div class=\"data\" >\r\n				    <div class=\"temperature\">\r\n				        <span><em>24</em></span>\r\n				        <sup>°</sup>\r\n				    </div>\r\n					<p>27℃~35℃</p>\r\n					<span class=\"today\">周一</span>\r\n				</div>\r\n				<div class=\"weather\">\r\n					<img src=\"resource/images/weather/01.png\">\r\n					<span style=\"\"><img src=\"resource/images/weather/01.png\" /><span class=\"otherday\"></span></span>\r\n					<span style=\"\"><img src=\"resource/images/weather/01.png\" /><span class=\"otherday\"></span></span>\r\n					<span style=\"\"><img src=\"resource/images/weather/01.png\" /><span class=\"otherday\"></span></span>\r\n				</div>\r\n			</div>\r\n		</div>\r\n	</div>\r\n</div>");
  
});

Ember.TEMPLATES["floor"] = Ember.Handlebars.template(function anonymous(Handlebars,depth0,helpers,partials,data) {
this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Ember.Handlebars.helpers); data = data || {};
  var buffer = '', stack1, helper, options, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = '', helper, options;
  data.buffer.push("\r\n			");
  data.buffer.push(escapeExpression((helper = helpers['store-detail'] || (depth0 && depth0['store-detail']),options={hash:{
    'store': ("currentShop"),
    'hideDetail': ("hideDetail"),
    'gotoIndex': ("gotoIndex")
  },hashTypes:{'store': "ID",'hideDetail': "STRING",'gotoIndex': "STRING"},hashContexts:{'store': depth0,'hideDetail': depth0,'gotoIndex': depth0},contexts:[],types:[],data:data},helper ? helper.call(depth0, options) : helperMissing.call(depth0, "store-detail", options))));
  data.buffer.push("\r\n		");
  return buffer;
  }

  data.buffer.push("<div class=\"wrap\">\r\n	<div class=\"r-page wrap-main\">\r\n		");
  stack1 = helpers['if'].call(depth0, "showStoreDetail", {hash:{},hashTypes:{},hashContexts:{},inverse:self.noop,fn:self.program(1, program1, data),contexts:[depth0],types:["ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\r\n		");
  stack1 = helpers._triageMustache.call(depth0, "top-weather", {hash:{},hashTypes:{},hashContexts:{},contexts:[depth0],types:["ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\r\n		");
  data.buffer.push(escapeExpression((helper = helpers['nav-bar'] || (depth0 && depth0['nav-bar']),options={hash:{
    'title': ("floor")
  },hashTypes:{'title': "STRING"},hashContexts:{'title': depth0},contexts:[],types:[],data:data},helper ? helper.call(depth0, options) : helperMissing.call(depth0, "nav-bar", options))));
  data.buffer.push("\r\n		<!--主体内容start-->\r\n		   <div id=\"mapContainer\" style=\"width: 1040px;height:1440px;position:relative;background: #F03;\">\r\n                  <!--<div id=\"mapContainer\" style=\"width: 520px;height: 720px;background: #F03;\">-->\r\n           </div>\r\n		<!--主体内容end-->\r\n		<div class=\"goStoreDetial\" ");
  data.buffer.push(escapeExpression(helpers.action.call(depth0, "showStoreDetail", {hash:{},hashTypes:{},hashContexts:{},contexts:[depth0],types:["STRING"],data:data})));
  data.buffer.push("></div>\r\n		<!--");
  data.buffer.push(escapeExpression((helper = helpers['bottom-map'] || (depth0 && depth0['bottom-map']),options={hash:{
    'shop': ("currentShop"),
    'setPaper': ("setPaper")
  },hashTypes:{'shop': "ID",'setPaper': "STRING"},hashContexts:{'shop': depth0,'setPaper': depth0},contexts:[],types:[],data:data},helper ? helper.call(depth0, options) : helperMissing.call(depth0, "bottom-map", options))));
  data.buffer.push("-->\r\n	</div>\r\n</div>\r\n\r\n\r\n");
  return buffer;
  
});

Ember.TEMPLATES["index"] = Ember.Handlebars.template(function anonymous(Handlebars,depth0,helpers,partials,data) {
this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Ember.Handlebars.helpers); data = data || {};
  var buffer = '', stack1, self=this;

function program1(depth0,data) {
  
  var buffer = '', stack1;
  data.buffer.push("\r\n			");
  stack1 = helpers._triageMustache.call(depth0, "bottom-map", {hash:{},hashTypes:{},hashContexts:{},contexts:[depth0],types:["ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\r\n		");
  return buffer;
  }

function program3(depth0,data) {
  
  var buffer = '', stack1;
  data.buffer.push("\r\n			");
  stack1 = helpers._triageMustache.call(depth0, "bottom-lead", {hash:{},hashTypes:{},hashContexts:{},contexts:[depth0],types:["ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\r\n		");
  return buffer;
  }

  data.buffer.push("<div class=\"con-wrap\">\r\n	<div class=\"wrap-main\">\r\n		");
  stack1 = helpers._triageMustache.call(depth0, "top-weather", {hash:{},hashTypes:{},hashContexts:{},contexts:[depth0],types:["ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\r\n		");
  stack1 = helpers._triageMustache.call(depth0, "nav-blocks", {hash:{},hashTypes:{},hashContexts:{},contexts:[depth0],types:["ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\r\n		");
  stack1 = helpers['if'].call(depth0, "person", {hash:{},hashTypes:{},hashContexts:{},inverse:self.program(3, program3, data),fn:self.program(1, program1, data),contexts:[depth0],types:["ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\r\n	</div>\r\n</div>");
  return buffer;
  
});

Ember.TEMPLATES["park"] = Ember.Handlebars.template(function anonymous(Handlebars,depth0,helpers,partials,data) {
this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Ember.Handlebars.helpers); data = data || {};
  var buffer = '', stack1, helper, options, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = '', helper, options;
  data.buffer.push("\r\n			");
  data.buffer.push(escapeExpression((helper = helpers['park-nav'] || (depth0 && depth0['park-nav']),options={hash:{
    'hideDetail': ("hideDetail"),
    'gotoIndex': ("gotoIndex"),
    'isUndergroundLot': ("isUndergroundLot"),
    'isMsg': ("isMsg"),
    'msg': ("msg"),
    'data': ("data"),
    'area': ("area")
  },hashTypes:{'hideDetail': "STRING",'gotoIndex': "STRING",'isUndergroundLot': "ID",'isMsg': "ID",'msg': "ID",'data': "ID",'area': "ID"},hashContexts:{'hideDetail': depth0,'gotoIndex': depth0,'isUndergroundLot': depth0,'isMsg': depth0,'msg': depth0,'data': depth0,'area': depth0},contexts:[],types:[],data:data},helper ? helper.call(depth0, options) : helperMissing.call(depth0, "park-nav", options))));
  data.buffer.push("\r\n		");
  return buffer;
  }

function program3(depth0,data) {
  
  var buffer = '', helper, options;
  data.buffer.push("\r\n			");
  data.buffer.push(escapeExpression((helper = helpers['static-detail'] || (depth0 && depth0['static-detail']),options={hash:{
    'hideDetail': ("hideDetail"),
    'gotoIndex': ("gotoIndex"),
    'isMsg': ("isMsg"),
    'msg': ("msg")
  },hashTypes:{'hideDetail': "STRING",'gotoIndex': "STRING",'isMsg': "ID",'msg': "ID"},hashContexts:{'hideDetail': depth0,'gotoIndex': depth0,'isMsg': depth0,'msg': depth0},contexts:[],types:[],data:data},helper ? helper.call(depth0, options) : helperMissing.call(depth0, "static-detail", options))));
  data.buffer.push("\r\n		");
  return buffer;
  }

  data.buffer.push("<div class=\"wrap\">\r\n	<div class=\"r-page wrap-main\">\r\n		");
  stack1 = helpers['if'].call(depth0, "showParkNav", {hash:{},hashTypes:{},hashContexts:{},inverse:self.noop,fn:self.program(1, program1, data),contexts:[depth0],types:["ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\r\n		");
  stack1 = helpers['if'].call(depth0, "isMsg", {hash:{},hashTypes:{},hashContexts:{},inverse:self.noop,fn:self.program(3, program3, data),contexts:[depth0],types:["ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\r\n		");
  stack1 = helpers._triageMustache.call(depth0, "top-weather", {hash:{},hashTypes:{},hashContexts:{},contexts:[depth0],types:["ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\r\n		");
  data.buffer.push(escapeExpression((helper = helpers['nav-bar'] || (depth0 && depth0['nav-bar']),options={hash:{
    'title': ("park")
  },hashTypes:{'title': "STRING"},hashContexts:{'title': depth0},contexts:[],types:[],data:data},helper ? helper.call(depth0, options) : helperMissing.call(depth0, "nav-bar", options))));
  data.buffer.push("\r\n		<!--主体内容start-->\r\n		<div class=\"park\" id=\"p-content\">\r\n			<div class=\"p-garage\" id=\"p-choose\">\r\n				<p>\r\n					<span>请选择您要去的车库</span>\r\n					<a href=\"javascript:void(0);\" data-name=\"lt\">立体车库</a>\r\n					<a href=\"javascript:void(0);\" class=\"checked\" data-name=\"underground\">地下车库</a>\r\n				</p>\r\n			</div>\r\n			<!--查找车位-->\r\n			<div class=\"p-garage\" style=\"display:none\" id=\"p-spot\">\r\n				<div class=\"p-search\">\r\n					<div class=\"p-back\" id=\"p-back\">\r\n					<a href=\"javascript:void(0);\">返回</a>\r\n					</div>\r\n					<br/>\r\n					<span><br/>请输入您的停车位号</span>\r\n					<div class=\"sure\">\r\n						<font>P</font>\r\n						<input type=\"text\" class=\"code\" id=\"parkingNum\" value=\"请输入停车位号\"/>\r\n						<a href=\"javascript:void(0);\" ");
  data.buffer.push(escapeExpression(helpers.action.call(depth0, "search", {hash:{
    'target': ("view")
  },hashTypes:{'target': "STRING"},hashContexts:{'target': depth0},contexts:[depth0],types:["STRING"],data:data})));
  data.buffer.push(">确认</a>\r\n					</div>\r\n					<span>例:108</span>\r\n				</div>\r\n				<div class=\"p-num\">\r\n					<ul>\r\n						<li t=\"n\">1</li>\r\n						<li t=\"n\">2</li>\r\n						<li t=\"n\">3</li>\r\n						<li t=\"n\">4</li>\r\n						<li t=\"n\">5</li>\r\n						<li t=\"n\">6</li>\r\n						<li t=\"n\">7</li>\r\n						<li t=\"n\">8</li>\r\n						<li t=\"n\">9</li>\r\n						<li t=\"n\">0</li>\r\n						<li class=\"null\" id=\"clearkey\"></li>\r\n						<li class=\"del\" id=\"deletekey\"></li>\r\n					</ul>\r\n				</div>\r\n			</div>\r\n		</div>\r\n		<!--主体内容end-->\r\n		");
  stack1 = helpers._triageMustache.call(depth0, "bottom-map", {hash:{},hashTypes:{},hashContexts:{},contexts:[depth0],types:["ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\r\n	</div>\r\n</div>\r\n");
  return buffer;
  
});

Ember.TEMPLATES["static"] = Ember.Handlebars.template(function anonymous(Handlebars,depth0,helpers,partials,data) {
this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Ember.Handlebars.helpers); data = data || {};
  var buffer = '', stack1, escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = '';
  data.buffer.push("\r\n							<div class=\"member-listDiv\">\r\n								<img ");
  data.buffer.push(escapeExpression(helpers['bind-attr'].call(depth0, {hash:{
    'src': ("model.src")
  },hashTypes:{'src': "ID"},hashContexts:{'src': depth0},contexts:[],types:[],data:data})));
  data.buffer.push(" />\r\n							</div>\r\n						");
  return buffer;
  }

  data.buffer.push("<div class=\"wrap\">\r\n	<div class=\"r-page wrap-main\">\r\n		");
  stack1 = helpers._triageMustache.call(depth0, "top-weather", {hash:{},hashTypes:{},hashContexts:{},contexts:[depth0],types:["ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\r\n		");
  stack1 = helpers._triageMustache.call(depth0, "nav-bar", {hash:{},hashTypes:{},hashContexts:{},contexts:[depth0],types:["ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\r\n		<!--主体内容start-->\r\n		<div class=\"main\">\r\n			<div class=\"m-con\">\r\n				<div class=\"member\">\r\n					<div class=\"member-list\">\r\n						");
  stack1 = helpers.each.call(depth0, "model", "in", "controller", {hash:{},hashTypes:{},hashContexts:{},inverse:self.noop,fn:self.program(1, program1, data),contexts:[depth0,depth0,depth0],types:["ID","ID","ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\r\n					</div>\r\n				</div>\r\n				<!--浮动按钮-->\r\n				<div class=\"m-con-but-left\">\r\n					<a id=\"pre\"></a>\r\n				</div>\r\n				<div class=\"m-con-but-right\">\r\n					<a id=\"next\"></a>\r\n				</div>\r\n				<!--浮动按钮end-->\r\n			</div>\r\n		</div>\r\n		<!--主体内容end-->\r\n	</div>\r\n</div>\r\n");
  return buffer;
  
});

Ember.TEMPLATES["stores"] = Ember.Handlebars.template(function anonymous(Handlebars,depth0,helpers,partials,data) {
this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Ember.Handlebars.helpers); data = data || {};
  var buffer = '', stack1, helper, options, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = '', helper, options;
  data.buffer.push("\r\n			");
  data.buffer.push(escapeExpression((helper = helpers['store-detail'] || (depth0 && depth0['store-detail']),options={hash:{
    'store': ("store"),
    'hideDetail': ("hideDetail"),
    'gotoIndex': ("gotoIndex")
  },hashTypes:{'store': "ID",'hideDetail': "STRING",'gotoIndex': "STRING"},hashContexts:{'store': depth0,'hideDetail': depth0,'gotoIndex': depth0},contexts:[],types:[],data:data},helper ? helper.call(depth0, options) : helperMissing.call(depth0, "store-detail", options))));
  data.buffer.push("\r\n		");
  return buffer;
  }

function program3(depth0,data) {
  
  var buffer = '', helper, options;
  data.buffer.push("\r\n			");
  data.buffer.push(escapeExpression((helper = helpers['static-detail'] || (depth0 && depth0['static-detail']),options={hash:{
    'article': ("article"),
    'isAddressMap': (true),
    'hideDetail': ("hideDetail"),
    'gotoIndex': ("gotoIndex")
  },hashTypes:{'article': "ID",'isAddressMap': "BOOLEAN",'hideDetail': "STRING",'gotoIndex': "STRING"},hashContexts:{'article': depth0,'isAddressMap': depth0,'hideDetail': depth0,'gotoIndex': depth0},contexts:[],types:[],data:data},helper ? helper.call(depth0, options) : helperMissing.call(depth0, "static-detail", options))));
  data.buffer.push("\r\n		");
  return buffer;
  }

function program5(depth0,data) {
  
  var buffer = '', helper, options;
  data.buffer.push("\r\n			");
  data.buffer.push(escapeExpression((helper = helpers['popup-map'] || (depth0 && depth0['popup-map']),options={hash:{
    'store': ("store"),
    'hidePopMap': ("hidePopMap"),
    'gotoIndex': ("gotoIndex")
  },hashTypes:{'store': "ID",'hidePopMap': "STRING",'gotoIndex': "STRING"},hashContexts:{'store': depth0,'hidePopMap': depth0,'gotoIndex': depth0},contexts:[],types:[],data:data},helper ? helper.call(depth0, options) : helperMissing.call(depth0, "popup-map", options))));
  data.buffer.push("\r\n		");
  return buffer;
  }

function program7(depth0,data) {
  
  var buffer = '', helper, options;
  data.buffer.push("\r\n						");
  data.buffer.push(escapeExpression((helper = helpers['item-overview'] || (depth0 && depth0['item-overview']),options={hash:{
    'store': ("store"),
    'showDetailCom': ("showDetailCom"),
    'showPopMap': ("showPopMap")
  },hashTypes:{'store': "ID",'showDetailCom': "STRING",'showPopMap': "STRING"},hashContexts:{'store': depth0,'showDetailCom': depth0,'showPopMap': depth0},contexts:[],types:[],data:data},helper ? helper.call(depth0, options) : helperMissing.call(depth0, "item-overview", options))));
  data.buffer.push("\r\n					");
  return buffer;
  }

function program9(depth0,data) {
  
  
  data.buffer.push("\r\n						no store\r\n					");
  }

  data.buffer.push("<div class=\"wrap\">\r\n	<div class=\"wrap-main r-page\">\r\n		");
  stack1 = helpers['if'].call(depth0, "showDetail", {hash:{},hashTypes:{},hashContexts:{},inverse:self.noop,fn:self.program(1, program1, data),contexts:[depth0],types:["ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\r\n		");
  stack1 = helpers['if'].call(depth0, "showAddress", {hash:{},hashTypes:{},hashContexts:{},inverse:self.noop,fn:self.program(3, program3, data),contexts:[depth0],types:["ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\r\n		");
  stack1 = helpers['if'].call(depth0, "showPopMap", {hash:{},hashTypes:{},hashContexts:{},inverse:self.noop,fn:self.program(5, program5, data),contexts:[depth0],types:["ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\r\n		\r\n		");
  stack1 = helpers._triageMustache.call(depth0, "top-weather", {hash:{},hashTypes:{},hashContexts:{},contexts:[depth0],types:["ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\r\n		");
  data.buffer.push(escapeExpression((helper = helpers['nav-bar'] || (depth0 && depth0['nav-bar']),options={hash:{
    'title': ("brand")
  },hashTypes:{'title': "STRING"},hashContexts:{'title': depth0},contexts:[],types:[],data:data},helper ? helper.call(depth0, options) : helperMissing.call(depth0, "nav-bar", options))));
  data.buffer.push("\r\n		<!--主体内容start-->\r\n		<div class=\"con\">\r\n			<div class=\"con-left\">\r\n				");
  data.buffer.push(escapeExpression((helper = helpers['category-list'] || (depth0 && depth0['category-list']),options={hash:{
    'categoryQuery': ("categoryQuery"),
    'showAddress': ("showAddress")
  },hashTypes:{'categoryQuery': "STRING",'showAddress': "STRING"},hashContexts:{'categoryQuery': depth0,'showAddress': depth0},contexts:[],types:[],data:data},helper ? helper.call(depth0, options) : helperMissing.call(depth0, "category-list", options))));
  data.buffer.push("\r\n			</div>\r\n			<!--主体左侧end-->\r\n			<div class=\"con-right\">\r\n				<div class=\"con-right-m\">\r\n					");
  stack1 = helpers.each.call(depth0, "store", "in", "model", {hash:{},hashTypes:{},hashContexts:{},inverse:self.program(9, program9, data),fn:self.program(7, program7, data),contexts:[depth0,depth0,depth0],types:["ID","ID","ID"],data:data});
  if(stack1 || stack1 === 0) { data.buffer.push(stack1); }
  data.buffer.push("\r\n				</div>\r\n			</div>\r\n			<!--主体右侧end-->\r\n		</div>\r\n		<!--主体内容end-->\r\n	</div>\r\n</div>\r\n\r\n");
  return buffer;
  
});